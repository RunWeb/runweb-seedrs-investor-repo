<a href="/franchise/<?=$franchise -> id;?>/invoices">Back to invoices</a>
<h1>Invoice</h1>
<dl>
	<dt>
		ID
	</dt>
	<dd>
		<?=HTML::chars($invoice -> id);?>
	</dd>
	<dt>
		franchise_id
	</dt>
	<dd>
		<?=HTML::chars($invoice -> franchise_id);?>
	</dd>
	<dt>
		invoiceno
	</dt>
	<dd>
		<?=HTML::chars($invoice -> invoiceno);?>
	</dd>
	<dt>
		version
	</dt>
	<dd>
		<?=HTML::chars($invoice -> version);?>
	</dd>
	<dt>
		date
	</dt>
	<dd>
		<?=HTML::chars($invoice -> date);?>
	</dd>
	<dt>
		status
	</dt>
	<dd>
		<?=HTML::chars($invoice -> status);?>
	</dd>
	<dt>
		datepaid
	</dt>
	<dd>
		<?=HTML::chars($invoice -> datepaid);?>
	</dd>
	<dt>
		vat
	</dt>
	<dd>
		<?=HTML::chars($invoice -> vat);?>
	</dd>
	<dt>
		notes
	</dt>
	<dd>
		<?=HTML::chars($invoice -> notes);?>
	</dd>
	<dt>
		template
	</dt>
	<dd>
		<?=HTML::chars($invoice -> template);?>
	</dd>
	<dt>
		franchise_address
	</dt>
	<dd>
		<?=nl2br(HTML::chars($invoice -> franchise_address));?>
	</dd>
	<dt>
		runweb_address
	</dt>
	<dd>
		<?=nl2br(HTML::chars($invoice -> runweb_address));?>
	</dd>
	<dt>
		total
	</dt>
	<dd>
		<?=HTML::chars($invoice -> total());?>
	</dd>
</dl>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>product name</th>
			<th>product description</th>
			<th>description</th>
			<th>type</th>
			<th>price</th>
		</tr>
	</thead>
	<tbody>
		<?php
foreach ($invoice -> invoice_lines -> find_all() -> as_array() as $line) :
		?>
		<tr>
			<td><?=HTML::chars($line -> id);?></td>
			<td><?=HTML::chars($line -> product -> franchise_product -> name);?></td>
			<td><?=HTML::chars($line -> product -> franchise_product -> description);?></td>
			<td><?=nl2br(HTML::chars($line -> description));?></td>
			<td><?=HTML::chars($line -> type);?></td>
			<td><?=HTML::chars($line -> price);?></td>
		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>
