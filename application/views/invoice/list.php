<a href="/franchise/<?=$franchise->id;?>">Back to franchise</a>
<h1>Invoices</h1>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>invoiceno</th>
			<th>version</th>
			<th>date</th>
			<th>status</th>
			<th>total</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if (!count($invoices)) :
			?>
			<tr>
				<td colspan="6">None found!</td>
			</tr>
			<?php
		endif;
		
foreach ($invoices as $invoice) :
		?>
		<tr>
			<td><?=(int)$invoice -> id;?></td>
			<td><?=HTML::chars($invoice -> invoiceno);?></td>
			<td><?=HTML::chars($invoice -> version);?></td>
			<td><?=HTML::chars($invoice -> date);?></td>
			<td><?=HTML::chars($invoice -> status);?></td>
			<td><?=HTML::chars($invoice -> total());?></td>
			<td><a href="/franchise/<?=(int)$franchise -> id;?>/invoice/<?=(int)$invoice -> id; ?>" class="btn btn-primary">View</a></td>
		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>