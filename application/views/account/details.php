<?php
$page = isset($page) ? $page : 'account/details';
$has = array();
$has['businessaddress'] = !empty($account -> businessaddress1) || !empty($account -> businessaddress2) || !empty($account -> businessaddress3) || !empty($account -> businessaddress4) || !empty($account -> businesspostcode);
$has['personaladdress'] = !empty($account -> personaladdress1) || !empty($account -> personaladdress2) || !empty($account -> personaladdress3) || !empty($account -> personaladdress4) || !empty($account -> personalpostcode);
?><h2>Account details</h2>
<?=HTML::anchor($page . '/edit', 'Edit', array('class' => 'btn btn-primary edit'));?>
<h3>User Details</h3>
<dl class="dl-horizontal">
	<dt>
		<?=HTML::chars($account -> labels('username')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> username);?>
	</dd>
	<dt>
		<?=HTML::chars($account -> labels('email')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> email);?>
	</dd>
	<dt>
		<?=HTML::chars($account -> labels('firstname')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> firstname);?>
	</dd>
	<dt>
		<?=HTML::chars($account -> labels('lastname')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> lastname);?>
	</dd>
	<?php if (!empty($account -> telephone)) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('telephone')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> telephone);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> mobile)) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('mobile')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> mobile);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> fax)) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('fax')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> fax);?>
	</dd>
	<?php endif;?>
	<?php if ($has['personaladdress']) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('personaladdress1')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> personaladdress1);?>
	</dd>
	<?php endif;?>

	<?php if (!empty($account -> personaladdress2)) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('personaladdress2')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> personaladdress2);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> personaladdress3)) :
	?>

	<dt>
		<?=HTML::chars($account -> labels('personaladdress3')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> personaladdress3);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> personaladdress4)) :
	?>

	<dt>
		<?=HTML::chars($account -> labels('personaladdress4')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> personaladdress4);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> personalpostcode)) :
	?>

	<dt>
		<?=HTML::chars($account -> labels('personalpostcode')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars(Filter::format_postcode($account -> personalpostcode));?>
	</dd>
	<?php endif;?>
</dl>
<h3>Company Info</h3>
<dl class="dl-horizontal">
	<?php if (!empty($account -> companyname)) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('companyname')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> companyname);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> companyno)) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('companyno')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> companyno);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> companyno)) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('companyno')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> companyno);?>
	</dd>
	<?php endif;?>
	<?php if ($has['businessaddress']) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('businessaddress1')) . ':';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> businessaddress1);?>
	</dd>
	<?php endif;?>

	<?php if (!empty($account -> businessaddress2)) :
	?>
	<dt>
		<?=HTML::chars($account -> labels('businessaddress2')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> businessaddress2);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> businessaddress3)) :
	?>

	<dt>
		<?=HTML::chars($account -> labels('businessaddress3')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> businessaddress3);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> businessaddress4)) :
	?>

	<dt>
		<?=HTML::chars($account -> labels('businessaddress4')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> businessaddress4);?>
	</dd>
	<?php endif;?>
	<?php if (!empty($account -> businesspostcode)) :
	?>

	<dt>
		<?=HTML::chars($account -> labels('businesspostcode')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars(Filter::format_postcode($account -> businesspostcode));?>
	</dd>
	<?php endif;?>
</dl>
<h3>Extra</h3>
<dl class="dl-horizontal">
	<?php if (!empty($account -> notes)) :
	?>

	<dt>
		<?=HTML::chars($account -> labels('notes')) . '&nbsp;';?>
	</dt>
	<dd>
		<?=HTML::chars($account -> notes);?>
	</dd>
	<?php endif;?>
</dl>