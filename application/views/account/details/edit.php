<?php
$labels = $account -> labels();
?>
<?=Form::open(NULL, array('class' => 'form-horizontal'));?>

<fieldset>
	<legend>Edit Account Details</legend>
		<h2>User Details</h2>
		<div class="control-group">
            <?=Form::label('username', $labels['username'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<span class="input-xlarge uneditable-input" title="You cannot change your username"><?=HTML::chars($account -> username);?></span>
        	</div>
      	</div>      	
		<div class="control-group">
            <?=Form::label('email', $labels['email'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<span class="input-xlarge uneditable-input" title="To change your email you must contact us directly"><?=HTML::chars($account -> email);?></span>
            	<p class="help-block">Username and email are integral to your account.</p>
        	</div>
      	</div>
      	<hr />
		<div class="control-group">
            <?=Form::label('firstname', $labels['firstname'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('firstname', $account -> firstname, $account -> attributes('firstname'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('lastname', $labels['lastname'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('lastname', $account -> lastname, $account -> attributes('lastname'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('telephone', $labels['telephone'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('telephone', $account -> telephone, $account -> attributes('telephone'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('mobile', $labels['mobile'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('mobile', $account -> mobile, $account -> attributes('mobile'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('fax', $labels['fax'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('fax', $account -> fax, $account -> attributes('fax'));?>
        	</div>
      	</div>
      	<hr />

		<div class="control-group">
            <?=Form::label('personaladdress1', $labels['personaladdress1'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('personaladdress1', $account -> personaladdress1, $account -> attributes('personaladdress1'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('personaladdress2', $labels['personaladdress2'] . '&nbsp;', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('personaladdress2', $account -> personaladdress2, $account -> attributes('personaladdress2'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('personaladdress3', $labels['personaladdress3'] . '&nbsp;', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('personaladdress3', $account -> personaladdress3, $account -> attributes('personaladdress3'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('personaladdress4', $labels['personaladdress4'] . '&nbsp;', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('personaladdress4', $account -> personaladdress4, $account -> attributes('personaladdress4'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('personalpostcode', $labels['personalpostcode'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('personalpostcode', $account -> personalpostcode, $account -> attributes('personalpostcode'));?>
        	</div>
      	</div>		

		<hr />
		<h2>Company Details</h2>
      	<div class="control-group">
            <?=Form::label('companyname', $labels['companyname'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('companyname', $account -> companyname, $account -> attributes('companyname'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('companyno', $labels['companyno'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('companyno', $account -> companyno, $account -> attributes('companyno'));?>
        	</div>
      	</div>
      	<div class="control-group">
            <?=Form::label('businessaddress1', $labels['businessaddress1'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('businessaddress1', $account -> businessaddress1, $account -> attributes('businessaddress1'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('businessaddress2', $labels['businessaddress2'] . '&nbsp;', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('businessaddress2', $account -> businessaddress2, $account -> attributes('businessaddress2'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('businessaddress3', $labels['businessaddress3'] . '&nbsp;', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('businessaddress3', $account -> businessaddress3, $account -> attributes('businessaddress3'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('businessaddress4', $labels['businessaddress4'] . '&nbsp;', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('businessaddress4', $account -> businessaddress4, $account -> attributes('businessaddress4'));?>
        	</div>
      	</div>
		<div class="control-group">
            <?=Form::label('businesspostcode', $labels['businesspostcode'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::input('businesspostcode', $account -> businesspostcode, $account -> attributes('businesspostcode'));?>
        	</div>
      	</div>
		<hr />
		<h2>Additional</h2>
		<div class="control-group">
            <?=Form::label('notes', $labels['notes'] . ':', array('class' => 'control-label'));?>
            <div class="controls">
            	<?=Form::textarea('notes', $account -> notes, $account -> attributes('notes'));?>
        	</div>
      	</div>
      	<div class="form-actions">
            <button type="submit" class="btn btn-primary">Save changes</button>
            <?=HTML::anchor('account/details', 'Cancel', array('class' => 'btn'));?>
        </div>
</fieldset>
<?=Form::close();?>

