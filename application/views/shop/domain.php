<?php
	$data = array('fullname' => $customer->fullname, 'email' => $customer->email, 'domain' => $customer->domain, 'time' => $customer->time, 'companynameno' => $customer->companynameno, 'type' => $customer->type, 'houseno' => $customer->houseno, 'street' => $customer->street, 'town' => $customer->town, 'county' => $customer->county, 'postcode' => $customer->postcode, 'telephone' => $customer->telephone, 'billinghouseno' => $customer->billinghouseno, 'billingstreet' => $customer->billingstreet, 'billingtown' => $customer->billingtown, 'billingcounty' => $customer->billingcounty, 'billingpostcode' => $customer->billingpostcode);
	foreach ($data as $key => $value) {
		if (empty($value) || $value == 'null') {
			unset($data[$key]);
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="http://shop.runweb.co.uk/kohana/" />
		<meta charset="UTF-8">
		<title>RunWeb - Buy a domain</title>
		<link rel="stylesheet" href="assets/domain.css" />
	</head>
	<body>
		<!-- start header -->
		<header style="width: 100%; height: 171px; background: #191919; text-align: right; background-repeat: repeat-x; background-position: top right;">
			<div style="width: 940px; margin: 0 auto;">
				<div style="float: left; margin: 10px 0px 0px 40px; text-align: left;">
					<a href="shop/domain">
					<img src="assets/images/logo.png" width="168" height="85" alt="RunWeb.co.uk Web Design" style="position: absolute; margin-top: 30px;"/>
					</a>
				</div>
				<div id="progress"></div>
				<a href="shop/domain#search">1</a>
				<a href="shop/domain#time">2</a>
				<a href="shop/domain#type">3</a>
				<a href="shop/domain#details">4</a>
				<a href="shop/domain#summary">5</a>
			</div>
		</header>
		<div id="container">
			<?=View::factory('shop/domain/search')->set('customer', $data).View::factory('shop/domain/time')->set('customer', $data).View::factory('shop/domain/type')->set('customer', $data).View::factory('shop/domain/details')->set('customer', $data).View::factory('shop/domain/summary')->set('customer', $data).View::factory('shop/domain/thanks')->set('customer', $data);?>
			<div style="clear:both">
				&nbsp;
			</div>
		</div>
		<!-- start footer -->
		<footer>
			<div>
				<div class="copy">
					<img src="http://www.runweb.co.uk/images/runweb-cms.jpg" alt="RunWeb | CMS">
				</div>
				<div class="copy">
					RunWeb  &copy; 2013
				</div>
			</div>
		</footer>
		<!-- end footer -->
		<script>
			var domain_data = <?=json_encode($data);?>;
		</script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
		<script src="assets/jquery.history.js"></script>
		<script src="assets/all.js"></script>
	</body>
</html>
