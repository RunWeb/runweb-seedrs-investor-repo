<!DOCTYPE html>
<html>
	<head>
		<base href="http://shop.runweb.co.uk/kohana/" />
		<title>RunWeb Shop</title>
	</head>
	<body>
		<h1>RunWeb Shop</h1>
		<p>
			<a href="shop/domain">Domain</a>
			<a href="shop/website">Website</a>
		</p>
		<form action="/kohana/shop/data" method="post">
		<h2>Your session details: </h2>
		<p>
			<input type="hidden" name="redirect" value="1" />
			<?php
			foreach (array('id', 'date', 'session', 'fullname', 'email', 'domain', 'time', 'companynameno', 'type','houseno', 'street', 'town', 'county', 'postcode', 'telephone', 'billinghouseno', 'billingstreet', 'billingtown', 'billingcounty', 'billingpostcode') as $ite) {
				echo "<label style=\"display:inline-block; width: 10em;\">$ite</label>: <input type=\"text\" name=\"$ite\" value=\"{$customer -> $ite}\"><br />";
			}
			?>
			<label style="display:inline-block; width: 10em;">template</label>: <input type="text" name="template" value="1" /><br />
			<label style="display:inline-block; width: 10em;">save</label>: <input type="Submit" value="Save" />
		</p>
		</form>
		<p>
			<form action="/kohana/shop/create" method="post" style="display:inline-block;">
				<input type="hidden" name="confirm" value="1" />
				<input type="hidden" name="type" value="joomla" />
				<input type="hidden" name="id" value="<?=$customer->id; ?>" />
				<input type="submit" value="Make joomla plan" />
			</form>
<!-- 			<form action="/kohana/plesk/add" method="post"style="display:inline-block;">
				<input type="hidden" name="confirm" value="1" />
				<input type="hidden" name="type" value="Unlimited" />
				<input type="hidden" name="id" value="<?=$customer->id; ?>" />
				<input type="submit" value="Make plan - Unlimited" />
			</form> -->
		</p>
	</body>
</html>