<!-- start domain search -->
<section id="domain-search">
	<p>
		Please search for your domain...
	</p>
	<form method="post" action="index.html">
		<input type="text" placeholder="your-domain.com" name="domain" id="domain-search-domain" class="search text" required autofocus spellcheck="false" value="<?=isset($customer['domain']) ? htmlentities($customer ['domain']) : '';?>" />
		<input type="submit" name="submit" value="Search" id="domain-search-button" class="search button" />
	</form>
	<div id="loader-text"></div>
	<div id="domain-form-output">
		&nbsp;
	</div>
	<div id="domain-search-failed">
		<h3>Sorry, <span class="domain-name">the searched domain name</span> is not available.</h3>
		<p id="domain-search-failed-alternatives">
			<a href="shop/domain#search" class="button">Find alternatives</a>
		</p>
		<p id="domain-search-failed-loading">
			<img src="assets/images/load.gif" alt="Loading..." />
		</p>
		<ul id="domain-search-suggestions"></ul>
	</div>
</section>
<!-- end domain search -->