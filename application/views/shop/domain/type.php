<!-- start domain type -->
<section id="domain-type">
	<form method="post" action="index.html">
		<p>
			Is <span class="domain-name">your domain</span> a
			<label class="type-selection selection button<?=isset($customer['type']) && $customer['type'] == "personal" ? ' selected' : '';?>" for="domain-type-personal">
				<span>Personal</span>
				<input type="radio" name="type" value="personal" id="domain-type-personal"<?=isset($customer['type']) && $customer['type'] == "personal" ? ' selected' : '';?> />
				<!-- To Personal details page -->
			</label>
			or
			<label class="type-selection selection button<?=isset($customer['type']) && $customer['type'] == "business" ? ' checked="checked"' : '';?>" for="domain-type-business">
				<span>Business</span>
				<input type="radio" name="type" value="business" id="domain-type-business"<?=isset($customer['type']) && $customer['type'] == "business" ? ' checked="checked"' : '';?> />
				<!-- To Business details page -->
			</label>
			site?
		</p>
	</form>
</section>
<!-- end domain type -->