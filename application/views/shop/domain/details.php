<!-- start domain details -->
<section id="domain-details">
	<p>
		Enter your details
	</p>
	<form method="post" action="domain#details">
		<label class="icon icon-name type-personal type-business">
			<input type="text" placeholder="Name" name="fullname" id="domain-details-fullname" spellcheck="false" value="<?=isset($customer['fullname']) ? htmlentities($customer['fullname']) : '';?>" />
		</label>
		<label class="icon icon-email type-personal type-business">
			<input type="text" placeholder="Email" name="email" id="domain-details-email" spellcheck="false" value="<?=isset($customer['email']) ? htmlentities($customer['email']) : '';?>" />
		</label>
		<label class="icon type-business">
			<input type="text" placeholder="Company Name / Number" name="companynameno" id="domain-details-companynameno" spellcheck="false" value="<?=isset($customer['companynameno']) && $customer['companynameno'] != "false" ? htmlentities($customer['companynameno']) : '';?>" />
		</label>
		<a href="shop/domain#details" id="findCompany" class="type-business button">Search</a>
		<div class="icon-address">
			<label class="icon type-personal type-business">
				<input type="text" placeholder="House No / Name" name="houseno" id="domain-details-houseno" spellcheck="false" value="<?=isset($customer['houseno']) ? htmlentities($customer['houseno']) : '';?>"/>
			</label>
			<label class="icon type-personal type-business">
				<input type="text" placeholder="Street Name" name="street" id="domain-details-street" spellcheck="false" value="<?=isset($customer['street']) ? htmlentities($customer['street']) : '';?>" />
			</label>
			<label class="icon type-personal type-business">
				<input type="text" placeholder="Town" name="town" id="domain-details-town" spellcheck="false" value="<?=isset($customer['town']) ? htmlentities($customer['town']) : '';?>" />
			</label>
			<label class="icon type-personal type-business">
				<input type="text" placeholder="County" name="county" id="domain-details-county" spellcheck="false" value="<?=isset($customer['county']) ? htmlentities($customer['county']) : '';?>" />
			</label>
			<label class="icon type-personal type-business">
				<input type="text" placeholder="Postcode" name="postcode" id="domain-details-postcode" class="search text" size="12" spellcheck="false" value="<?=isset($customer['postcode']) ? htmlentities($customer['postcode']) : '';?>" />
			</label>
			<a href="shop/domain#details" id="findAddress" class="type-personal button">Search</a>
			<input type="submit" value="Done" class="button" id="domain-details-submit" />
		</div>
	</form>
</section>
<!-- end domain details -->