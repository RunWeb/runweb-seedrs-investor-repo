<!-- start domain time -->
<section id="domain-time">
	<form method="post" action="index.html">
		<p>
			Would you like <span class="domain-name">your domain name</span> for
			<br />
			<label class="year-selection selection button<?=isset($customer['time']) && $customer['time'] == 1 ? ' selected' : '';?>" for="domain-time-1year">
				<span>1 year</span> <span class="price">&pound;6.99</span> <span class="vat">inc VAT</span>
				<input type="radio" name="time" value="1" id="domain-time-1year"<?=isset($customer['time']) && $customer['time'] == 1 ? ' checked="checked"' : '';?> />
			</label>
			,
			<label class="year-selection selection button<?=isset($customer['time']) && $customer['time'] == 2 ? ' selected' : '';?>" for="domain-time-2year">
				<span>2 years</span> <span class="price">&pound;9.99</span> <span class="vat">inc VAT</span>
				<input type="radio" name="time" value="2" id="domain-time-2year"<?=isset($customer['time']) && $customer['time'] == 2 ? ' checked="checked"' : '';?> />
			</label>
			or
			<label class="year-selection selection button<?=isset($customer['time']) && $customer['time'] == 3 ? ' selected' : '';?>" for="domain-time-3year">
				<span>3 years</span> <span class="price">&pound;14.99</span> <span class="vat">inc VAT</span>
				<input type="radio" name="time" value="3" id="domain-time-3year"<?=isset($customer['time']) && $customer['time'] == 3 ? ' checked="checked"' : '';?> />
			</label>
			?
		</p>
	</form>
</section>
<!-- end domain time -->