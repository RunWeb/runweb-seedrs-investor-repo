<?php
$differentAddress = !empty($customer['postcode']) && !empty($customer['houseno']);
?>
<!-- start domain summary -->
<section id="domain-summary" style="text-align: left;">
	<form method="post" action="/kohana/shop/domain">
	<div class="column">
		<p style="text-indent: -1em;">
			<span class="domain-name">runweb2.co.uk</span>
		</p>
		<div class="type-business type-personal">
			<span id="domain-summary-type">Personal account</span> for <span id="domain-summary-time">3</span> year(s), &pound;<span id="domain-summary-price">3.00</span><span style="font-size:0.8em;font-variant:small-caps">pa</span>
		</div>
		<div class="type-business type-personal">
			Total: &pound;<span id="domain-summary-total">9.00</span><span style="font-size:0.8em;font-variant:small-caps">inc VAT</span>
		</div>
		<div style="text-indent: -1em;font-weight: bold;color:#92b021;">
			Contact details
		</div>
		<div id="domain-summary-fullname" class="type-personal">
			
		</div>
		<div id="domain-summary-email" class="type-personal type-business">
			
		</div>
		<div class="type-personal type-business">
			<span id="domain-summary-houseno">,</span>&nbsp;<span id="domain-summary-street"></span>
		</div>
		<div id="domain-summary-town" class="type-personal type-business">
			
		</div>
		<div id="domain-summary-county" class="type-personal type-business">
			
		</div>
		<div id="domain-summary-postcode" class="type-personal type-business">
			
		</div>
	</div>
	<div class="column">
		<div style="text-indent: -1em;font-weight: bold;color:#92b021;">
			Billing details
		</div>

			<div>
				<label>
					My billing address is:</label><span style="display:inline-block;vertical-align:text-top;">
					<label class="radiolabel">
						<input type="radio"<?=$differentAddress?'':' checked="checked"';?> name="billing" onchange="if (this.checked) $('.differentAddress').fadeOut();" />
						My personal address
					</label>
					<br />
					<label class="radiolabel">
						<input type="radio"<?=$differentAddress?' checked="checked"':'';?> name="billing" id="domain-billing-different" onchange="if (this.checked) $('.differentAddress').fadeIn();" />
						A different address
					</label></span>
			</div>
			<div class="differentAddress"<?=$differentAddress?' style="display:block"':'';?>>
				<label>
					House no: </label>
				<span>
					<input type="text"  class="text" placeholder="House no /name" id="domain-billing-houseno" value="<?=isset($customer ['billinghouseno']) ? htmlentities($customer['houseno']) : '';?>"  />
				</span>
			</div>
			<div class="differentAddress"<?=$differentAddress?' style="display:block"':'';?>>
				<label>
					Street name: </label>
				<span>
					<input type="text" class="text" placeholder="Street name" id="domain-billing-street" value="<?=isset($customer['billingstreet']) ? htmlentities($customer['billingstreet']) : '';?>"  />
				</span>
			</div>
			<div class="differentAddress"<?=$differentAddress?' style="display:block"':'';?>>
				<label>
					City/Town: </label>
				<span>
					<input type="text"  class="text" placeholder="City / Town" id="domain-billing-city" value="<?=isset($customer['billingtown']) ? htmlentities($customer['billingtown']) : '';?>"  />
				</span>
			</div>
			<div class="differentAddress"<?=$differentAddress?' style="display:block"':'';?>>
				<label>
					County: </label>
				<span>
					<input type="text"  class="text" placeholder="County" id="domain-billing-county" value="<?=isset($customer['billingcounty']) ? htmlentities($customer['billingcounty']) : '';?>" />
				</span>
			</div>
			<div class="differentAddress"<?=$differentAddress?' style="display:block"':'';?>>
				<label>
					Postal Code: </label>
				<span>
					<input type="text"  class="text" style="width:10em"  placeholder="Postal code" id="domain-billing-postcode" value="<?=isset($customer['billingpostcode']) ? htmlentities($customer['billingpostcode']) : '';?>" />
					<a href="shop/domain#summary" class="button" id="findAddress2" style="font-size:0.8em">Search</a></span>
			</div>
			<div>
				<label>
					Telephone number: </label>
				<span>
					<input type="text"  class="text"  placeholder="Telephone number" id="domain-billing-telephone" value="<?=isset($customer['telephone']) ? htmlentities($customer['telephone']) : '';?>" />
				</span>
			</div>
			<div style="text-indent: -1em;font-weight: bold;color:#92b021;">
				Card details
			</div>
			<div>
				<label>
					Card number: </label>
				<span>
					<input type="text"  class="text" placeholder="Card number" id="domain-billing-card"  />
				</span>
			</div>
			<div>
				<label>
					Expiration date: </label>
				<span>
					<select id="domain-billing-expiration-month">
						<option value="0">Month</option><!-- TODO: Auto generate these lists -->
						<option value="1">Jan</option>
						<option value="2">Feb</option>
						<option value="3">Mar</option>
						<option value="4">Apr</option>
						<option value="5">May</option>
						<option value="6">Jun</option>
						<option value="7">Jul</option>
						<option value="8">Aug</option>
						<option value="9">Sep</option>
						<option value="10">Oct</option>
						<option value="11">Nov</option>
						<option value="12">Dec</option>
					</select>
					<select id="domain-billing-expiration-year">
						<option value="0">Year</option>
						<option>2011</option>
						<option>2012</option>
						<option>2013</option>
						<option>2014</option>
						<option>2015</option>
						<option>2016</option>
						<option>2017</option>
						<option>2018</option>
						<option>2019</option>
						<option>2020</option>
						<option>2021</option>
						<option>2022</option>
					</select> CSC:
					<input type="text" size="3" style="width:5em;" placeholder="CSC" id="domain-billing-csc" />
				</span>
			</div>
			<div>
				<label for="domain-billing-cardholder">
					Card holder name: </label>
				<span>
					<input type="text" class="text" placeholder="Full cardholder name" id="domain-billing-cardholder" />
				</span>
			</div>
	</div>
	<div style="text-indent: -1em;font-weight: bold;color:#92b021;clear: both;">
		Total: &pound;<span id="domain-summary-total">9.00</span><span style="font-size:0.8em;font-variant:small-caps">inc VAT</span>
	</div>
	<div>
		<label style="width:50%; text-align: left;" for="domain-billing-terms">
			Accept terms and conditions?
			<input type="checkbox" value="Buy" id="domain-billing-terms" />
			
		</label>
		<span class="errormessage" id="domain-billing-terms-message">&nbsp;</span>
	</div>
	<div><input type="submit" value="Purchase domain" class="button" id="domain-billing-purchase" /></div>
	</form>
</section>
<!-- end domain summary -->