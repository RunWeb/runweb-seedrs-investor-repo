<a href="/franchise/<?=$franchise -> id;?>">Back to franchise</a>
<h1>Customers</h1>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>username</th>
			<th>email</th>
			<th>last login</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php
if (!count($users)) :
		?>
		<tr>
			<td colspan="1">None found!</td>
		</tr>
		<?php
		endif;

		foreach ($users as $user) :
		?>
		<tr>
			<td><?=HTML::chars($user -> id);?></td>
			<td><?=HTML::chars($user -> firstname . " " . $user -> lastname);?></td>
			<td><?=HTML::chars($user -> username);?></td>
			<td><?=HTML::chars($user -> email);?></td>
			<td><?=HTML::chars($user -> last_login);?></td>
			<td><a href="/franchise/<?=$franchise -> id;?>/customer/<?=$user -> id;?>" class="btn btn-primary">View</a></td>
		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>