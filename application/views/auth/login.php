<!DOCTYPE html>
<html>
	<head>
		<title>Login runweb</title>
		<link rel="stylesheet/less" type="text/css" href="/assets/css/bootstrap/bootstrap.less">		
		<style type="text/less">
            html > body {
                background-color: #3B3B3B;
            }
            h1 {
            	width: 220px;
            	margin: 0 auto;
            	padding-left: 10px;
            }
            #login-box {
                width: 400px;
                text-align: left;
                margin: 4em auto;
                background-color: #FFFFFF;
                border: 1px solid rgba(0, 0, 0, 0.05);
                border-radius: 4px 4px 4px 4px;
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05) inset;
                margin-bottom: 20px;
                min-height: 20px;
                padding: 8px 20px;
                #login-text {
            		margin: 0 auto;
            		width: 230px;
            		form {
            			width: 220px;
		                padding: 10px;
		                margin: 0 auto;
		                input[type=submit] {
		                	width: 100%;
		                }
            		}
            		.alert {
            			margin-top: 9px;
            		}
            	}
            }
            #remember-me {
            	color: #9a9a9a;
            	cursor: pointer;
            }
		</style>
		<script src="/assets/js/less-1.3.0.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div id="login-box" class="well">
			<div id="login-text">
				<h1><?=isset($role) ? HTML::chars($role) : 'login';?></h1>
				<form action="/auth/login" method="post">
					<input type="text" name="username" placeholder="Username" value="<?=isset($username) ? HTML::chars($username) : '';?>" />
					<br />
					<input type="password" name="password" placeholder="Password" value="<?=isset($password) ? HTML::chars($password) : '';?>" />
					<br />
					<?=Form::hidden('csrf', Security::token())
					?>
					<label id="remember-me" class="checkbox" >
						<input type="checkbox" value="1" name="remember" />
						Remember me </label>
					<input type="submit" value="Log In" class="btn btn-primary btn-large" />
					<?=isset($message) ? '<div class="alert alert-error">' .HTML::chars($message).'</div>' : '';?>
				</form>
			</div>
		</div>
		<script src="/assets/js/jquery-1.7.2.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
	</body>
</html>
