<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Description</th>
			<th>Base Price</th>
			<th>RRP</th>
			<th>Enabled</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php
foreach ($products as $product) :
		$faps = $product -> franchise_available_products -> find_all() -> as_array();
		$franchise_products = $product -> franchise_products -> find_all() -> as_array();
		$franchises = array();
		$enabled_franchises = array();
		$fap_franchises = array();
		foreach ($faps as $fap) :
			$fap_franchises[$fap -> franchise -> id] = true;
			$franchises[$fap -> franchise -> id] = $fap -> franchise -> name;
		endforeach;
		foreach ($franchise_products as $fp) :
			$enabled_franchises[$fp -> franchise -> id] = true;
		endforeach;


		?>
		<tr>
			<td class="id"><?=$product -> id;?></td>
			<td class="name"><?=HTML::chars($product -> name);?></td>
			<td class="description"><?=$product -> description;?></td>
			<td class="baseprice">&pound;<?=sprintf("%.2f", $product -> baseprice);?></td>
			<td class="rrp">&pound;<?=sprintf("%.2f", $product -> rrp);?></td>
			<td class="enabled"><?=$product -> enabled ? 'YES' : 'NO';?></td>
			<td>
			<form class="inline" action="/products/disable" method="post">
				<input type="hidden" name="id" value="<?=$product -> id;?>" />
				<input type="hidden" name="enabled" value="<?=$product -> enabled ? 0 : 1;?>" />
				<a href="/products/edit/<?=$product -> id;?>" class="btn">Edit</a>
				<button type="submit" class="btn btn-<?=$product -> enabled ? 'success' : 'danger';?>">
					<?=$product -> enabled ? 'Enabled' : 'Disabled';?>
				</button>
			</form></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="6">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Franchise</th>
						<th>Enabled</th>
						<th>Available</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($franchises as $id => $franchise) :
					?>
					<tr>
						<td><?=$franchise;?></td>
						<td><?=$enabled_franchises[$id];?></td>
						<td><?=$fap_franchises[$id];?></td>
					</tr>
					<?php endforeach;?>
				</tbody>
			</table></td>
		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>
<a href="/products/add" class="btn btn-success">Add New</a>
