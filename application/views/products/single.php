<?=Form::open($add ? 'products/add': 'products/edit/'.$product -> id, array('class' => 'form-horizontal'));?>
<fieldset>
	<legend>
		Edit Product Details
	</legend>
	<input type="hidden" name="id" value="<?=$product -> id;?>" />
	<div class="control-group">
		<?=Form::label('name', 'Name:', array('class' => 'control-label'));
		?>
		<div class="controls">
			<?=Form::input('name', $product -> name);?>
		</div>
	</div>
	<div class="control-group">
		<?=Form::label('baseprice', 'Base Price:', array('class' => 'control-label'));
		?>
		<div class="controls">
			<div class="input-prepend">
                <span class="add-on">&pound;</span><?=Form::input('baseprice', sprintf("%.2f", $product -> baseprice));?>
             </div>
		</div>
	</div>
	<div class="control-group">
		<?=Form::label('rrp', 'RRP:', array('class' => 'control-label'));
		?>
		<div class="controls">
			<div class="input-prepend">
                <span class="add-on">&pound;</span><?=Form::input('rrp', sprintf("%.2f", $product -> rrp));?>
             </div>
		</div>
	</div>
	<div class="control-group">
		<?=Form::label('description', 'Description:', array('class' => 'control-label'));
		?>
		<div class="controls">
			<?=Form::textarea('description', $product -> description);?>
		</div>
	</div>
	<div class="control-group">
		<?=Form::label('enabled', 'Enabled:', array('class' => 'control-label'));
		?>
		<div class="controls">
			<?=Form::checkbox('enabled', 1, (bool) $product -> enabled);?>
		</div>
	</div>
	<div class="form-actions">
		<?php if ($add) : ?>
		<button type="submit" class="btn btn-success">
			Add Product
		</button>
		<?php else : ?>
			<button type="submit" class="btn btn-primary">
				Save changes
			</button>
		<?php endif; ?>
		<?=HTML::anchor('products/', 'Cancel', array('class' => 'btn'));?>
	</div>
</fieldset>
<?=Form::close();?>
