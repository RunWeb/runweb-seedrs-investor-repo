<?php /* @indent 0 */?>
<form action="/kohana/" method="post">
	<fieldset>
		<legend>
			New Customer
		</legend>
		<div class="form_row<?=isset($errors['username']) ? ' error_form_row' : '';?>">
			<label for="forms_customer_username">Username:</label>
			<input type="text" name="username" id="forms_customer_username" placeholder="Username" value="<?=isset($values['username']) ? htmlentities($values['username']) : '';?>" />
			<?php /* Errors for username */ if (isset($errors['username'])) :
			?><span class="error_field"><?=$errors['username']['message'];?></span><?php endif;?> 
		</div>
		<div class="form_row<?=isset($errors['name']) ? ' error_form_row' : '';?>">
			<label for="forms_customer_name">Name:</label>
			<input type="text" name="name" id="forms_customer_name" placeholder="Full name" value="<?=isset($values['name']) ? htmlentities($values['name']) : '';?>" />
			<?php /* Errors for name */ if (isset($errors['name'])) :
			?><span class="error_field"><?=$errors['name']['message'];?></span><?php endif;?> 
		</div>
		<div class="form_row<?=isset($errors['email']) ? ' error_form_row' : '';?>">
			<label for="forms_customer_email">Email:</label>
			<input type="text" name="email" id="forms_customer_email" placeholder="Email address" value="<?=isset($values['email']) ? htmlentities($values['email']) : '';?>" />
			<?php /* Errors for email */ if (isset($errors['email'])) :
			?><span class="error_field"><?=$errors['email']['message'];?></span><?php endif;?> 
		</div>
		<div class="form_row<?=isset($errors['password']) ? ' error_form_row' : '';?>">
			<label for="forms_customer_password">Password:</label>
			<input type="password" name="password" id="forms_customer_password" placeholder="Password" value="<?=isset($values['password']) ? htmlentities($values['password']) : '';?>" />
			<?php /* Errors for password */ if (isset($errors['password'])) :
			?><span class="error_field"><?=$errors['password']['message'];?></span><?php endif;?> 
		</div>
		<div class="form_row<?=isset($errors['passwordconfirm']) ? ' error_form_row' : '';?>">
			<label for="forms_customer_passwordconfirm">Confirm:</label>
			<input type="password" name="passwordconfirm" id="forms_customer_passwordconfirm" placeholder="Confirm password" value="<?=isset($values['passwordconfirm']) ? htmlentities($values['passwordconfirm']) : '';?>" />
			<?php /* Errors for username */ if (isset($errors['passwordconfirm'])) :
			?><span class="error_field"><?=$errors['passwordconfirm']['message'];?></span><?php endif;?> 
		</div>
		<div class="form_row<?=isset($errors['phone']) ? ' error_form_row' : '';?>">
			<label for="forms_customer_phone">Phone:</label>
			<input type="text" name="phone" id="forms_customer_phone" placeholder="e.g. 01277 236 200" value="<?=isset($values['phone']) ? htmlentities($values['phone']) : '';?>" />
			<?php /* Errors for phone */ if (isset($errors['phone'])) :
			?><span class="error_field"><?=$errors['phone']['message'];?></span><?php endif;?> 
		</div>
		<div class="form_row">
			<input type="submit" value="Create customer" />
		</div>
	</fieldset>
</form>