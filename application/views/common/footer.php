		</div>

		<script src="/assets/js/jquery-1.7.2.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<?php
		if(isset($scripts) && is_array($scripts)) {
			foreach ($scripts as $script) {
				echo '<script src="'.$script.'"></script>' . PHP_EOL;
			}
		}
		if(isset($inline_scripts)) :
			if (is_array($inline_scripts)) {
				foreach ($inline_scripts as $script) {
					echo $script . PHP_EOL;
				}
			} else {
				echo $inline_scripts . PHP_EOL;
			}
		endif;
		?>
		<script>
		$(function(){
			$('#benchmarking .btn').click(function() {
				$('#benchmarking .kohana .profiler').toggle();
			});
		});
		</script>
		<div id="benchmarking">
			<button class="btn btn-small">Benchmark</button>
			<?=View::factory('profiler/stats');?>
		</div>
	</body>
</html>