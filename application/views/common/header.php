<!DOCTYPE html>
<html>
	<head>
		<title><?=isset($title) ? $title : 'Shop';?></title>
		<link rel="stylesheet/less" type="text/css" href="/assets/css/bootstrap/bootstrap.less">
		<link rel="stylesheet/less" type="text/css" href="/assets/css/cp.less">
		<script src="/assets/js/less-1.3.0.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<ul class="nav pull-right nav-pills">
						<?php if (isset($user)) :
						?><li><a href="/account/details" class="pull-right"><?=$user -> username;?></a></li><?php endif;?>
					  <li>
					    <a href="/auth/logout" class="pull-right"><i class="icon-off icon-white"></i> Log Out</a>
					  </li>
					</ul>
					<hgroup>
						<h1><a class="brand" href="/">RunWeb</a></h1>
						<?php if (isset($title)) :
						?><h2><?=$title;?></h2><?php endif;?>
					</hgroup>
				</div>
			</div>
		</div>
		<div class="container">
