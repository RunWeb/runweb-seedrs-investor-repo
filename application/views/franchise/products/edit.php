<?=Form::open('franchise/' . $franchise -> id . '/products/' . $product -> id . '/edit', array('class' => 'form-horizontal'));?>
<?php
if (isset($message)) :
?>
<?=$message;?>
<?php
endif;
?>
<fieldset>
	<legend>
		Edit Product Details
	</legend>
	<div class="control-group">
		<?=Form::label('name', $product -> labels('name') . ':', array('class' => 'control-label'));?>
		<div class="controls">
			<?=Form::input('name', $product -> name(), $product -> attributes('name'));?>
		</div>
	</div>
	<div class="control-group">
		<?=Form::label('description', $product -> labels('description') . ':', array('class' => 'control-label'));?>
		<div class="controls">
			<?=Form::textarea('description', $product -> description(), $product -> attributes('description'));?>
		</div>
	</div>
	<div class="control-group">
		<?=Form::label('price', $product -> labels('price') . ':', array('class' => 'control-label'));?>
		<div class="controls">
			<?=Form::input('price', $product -> price(), $product -> attributes('price'));?>
		</div>
	</div>
	<div class="form-actions">
		<input type="hidden" name="id" value="<?=$product -> id;?>" />
		<button type="submit" class="btn btn-primary">
			Save changes
		</button>
		<?=HTML::anchor('franchise/' . $franchise -> id . '/products', 'Cancel', array('class' => 'btn'));?>
	</div>
</fieldset>
<?=Form::close();?>

