<h1>Franchises</h1>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Franchise</th>
			<th>Contact info</th>
			<th>Cust.</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php
foreach ($franchises as $franchise) :
		?>
		<tr>
			<td><?=(int)$franchise -> id;?></td>
			<td><?=HTML::chars($franchise -> name);?></td>
			<td><?=nl2br(HTML::chars($franchise -> contactinfo));?></td>
			<td><?=(int)$franchise -> customers -> count_all();?></td>
			<td><a href="/franchise/<?=(int)$franchise -> id;?>" class="btn btn-primary">View</a></td>
		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>