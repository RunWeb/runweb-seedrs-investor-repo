<hgroup>
	<h1>Franchise</h1>
	<h2><?=HTML::chars($franchise -> name); ?></h2>
</hgroup>
<div class="details">
	<h3>Details</h3>
	<dl class="dl-horizontal">
		<dt>Name</dt>
		<dd><?=HTML::chars($franchise -> name); ?></dd>
		<dt>Contact info</dt>
		<dd><?=nl2br(HTML::chars($franchise -> contactinfo)); ?></dd>
	</dl>
	<a href="/franchise/<?=(int)$franchise ->id; ?>/edit" class="btn btn-primary">Edit</a>
</div>
<div class="details">
	<h3>Products</h3>
	<a href="/franchise/<?=(int)$franchise ->id; ?>/products" class="btn btn-primary">Products</a>
</div>

<div class="details">
	<h3>Invoices</h3>
	<a href="/franchise/<?=(int)$franchise ->id; ?>/invoices" class="btn btn-primary">Invoices</a>
</div>
<div class="details">
	<h3>Templates</h3>
	<a href="/franchise/<?=(int)$franchise ->id; ?>/templates" class="btn btn-primary">Templates</a>
</div>
<div class="details">
	<h3>Users</h3>
	<a href="/franchise/<?=(int)$franchise ->id; ?>/users" class="btn btn-primary">Users</a>
</div>
<div class="details">
	<h3>Customers</h3>
	<a href="/franchise/<?=(int)$franchise ->id; ?>/customers" class="btn btn-primary">Customers</a>
</div>