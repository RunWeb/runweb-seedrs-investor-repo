<?=Form::open('franchise/' . $franchise -> id . '/edit', array('class' => 'form-horizontal'));?>
<?php 
if (isset($message)) :
?>
	<?=$message;?>
<?php
endif;
?>
<fieldset>
	<legend>
		Edit Franchise Details
	</legend>
	<div class="control-group">
		<?=Form::label('name', $franchise -> labels('name') . ':', array('class' => 'control-label'));?>
		<div class="controls">
			<?=Form::input('name', $franchise -> name, $franchise -> attributes('name'));?>
		</div>
	</div>
	<div class="control-group">
		<?=Form::label('contactinfo', $franchise -> labels('contactinfo') . ':', array('class' => 'control-label'));?>
		<div class="controls">
			<?=Form::textarea('contactinfo', $franchise -> contactinfo, $franchise -> attributes('contactinfo'));?>
		</div>
	</div>
	<div class="form-actions">
		<input type="hidden" name="action" value="edit" />
		<input type="hidden" name="id" value="<?=$franchise -> id;?>" />		
		<button type="submit" class="btn btn-primary">
			Save changes
		</button>
		<?=HTML::anchor('franchise/' . $franchise -> id, 'Cancel', array('class' => 'btn'));?>
	</div>
</fieldset>
<?=Form::close();?>

