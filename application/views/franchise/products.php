<a href="/franchise/<?=$franchise -> id;?>">Back to franchise</a>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th rowspan="2">Product</th>
			<th rowspan="2">Base Price</th>
			<th rowspan="2">RRP</th>
			<th rowspan="2">Price</th>
			<th colspan="2">Difference</th>
			<th rowspan="2">Description</th>
			<th rowspan="2">Enabled</th>
			<th rowspan="2">&nbsp;</th>
		</tr>
		<tr>
			<th>Base</th>
			<th>RRP</th>
		</tr>
	</thead>
	<tbody>
		<?php
foreach ($products as $product):

		?>
		<tr>
			<td><?=$product -> name();?></td>
			<td><?=$product -> baseprice();?></td>
			<td><?=$product -> rrp();?></td>
			<td><?=$product -> price();?></td>
			<td><?=$product -> profit();?></td>
			<td><?=$product -> rrpdifference();?></td>
			<td><?=$product -> description();?></td>
			<td>
			<form class="inline" action="/franchise/<?=$franchise -> id;?>/product/<?=$product -> id;?>/enable" method="post">
				<input type="hidden" name="id" value="<?=$product -> id;?>">
				<input type="hidden" name="enabled" value="<?=$product -> enabled();?>">
				<button type="submit" class="btn btn-<?=$product -> enabled() ? 'success' : 'danger';?> <?=!$product -> product -> enabled ? 'disabled' : '';?>" <?=!$product -> product -> enabled ? 'disabled="disabled"' : '';?>>
					<?=$product -> enabled() ? 'Enabled' : 'Disabled';?>
				</button>
			</form></td>
			<td>
				<a href="/franchise/<?=$franchise -> id;?>/product/<?=$product -> id;?>/edit" class="btn btn-primary">Edit</a>
		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>