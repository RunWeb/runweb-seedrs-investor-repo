<h1>Customers</h1>
<?php
//Debug::vars($user, $franchise, $customers);

if (!empty($customers)) :
	foreach ($customers as $customer) :
		echo View::factory('franchise/customers/single') -> bind('customer', $customer);
	endforeach;
endif;
