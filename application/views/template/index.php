<style>
    .template {
        width: 300px;
        float: left;
        border: 1px solid black;
        text-align: center;
        margin: 10px;
    }
    .title {
        background-color: #000;
        color: #fff;
    }
    span.color {
        display: inline-block;
        width: 30px;
        height: 30px;
        border: 1px solid black;
        outline: 1px solid white;
    }    span.tag {
        display: inline-block;
        padding: 3px;
        border: 1px solid black;margin 15px 1px;
    }
    div.palette {
        margin: 10px 0;
    }
    div.description {
    	font-size: small;
    }
    .new {
    	clear: both;
    }
    .palette {
    	cursor: pointer;
    }
</style>
<?php
if (!empty($message)) :
?>
<div class="success">
	<?=$message;?>
</div>
<?php
endif;
?>
<div class="new"><a href="/templates/install" class="btn btn-primary">Install new file</a></div>
<div id="templates">
<?php

foreach ($templates as $template) :
?>
<div class="template" data-packagename="<?=$template -> packagename;?>" data-version="<?=str_replace('.', '-', $template -> version);?>">
	<div class="title" title="Version <?=$template->version;?>">
		<?=$template -> name;?>
	</div>
	<img src="/assets/images/designs/<?=$template -> packagename;?>/<?=str_replace('.', '-', $template -> version);?>_1_small.png" />
	<div class="author">
		By <?=$template -> author;?>
	</div>
	<div class="tags">
		<?php
foreach ($template->template_tags->find_all() as $tag) :
		?>
		<span class="tag"><?=$tag -> tag;?></span>
		<?php
		endforeach;
		?>
	</div>
	<div class="palettes">
		<?php
foreach ($template->template_palettes->find_all() as $palette)  :
		?>
		<div class="palette" data-number="<?=$palette -> number;?>">
			Palette <?=$palette -> number;?>
			<?php
foreach ($palette->template_colors->order_by('importance')->find_all() as $color) :
			?>
			<span class="color" style="background-color:#<?=$color -> hex;?>" title="<?=$color -> name;?>">&nbsp;</span>
			<?php
			endforeach;
			?>
		</div>
		<?php
		endforeach;
		?>
	</div>
	<div class="description">
		<?=$template -> description;?>
	</div>
	<?=Form::open(NULL, array('class' => 'form-inline'));?>
	<input type="hidden" name="id" value="<?=$template->id;?>" />
	<?php if ($template->has('franchises', $franchise)) : ?>
		<input type="hidden" name="selected" value="0" />
		<button type="submit" class="btn btn-danger">Deselect</button>
	<?php else : ?>
		<input type="hidden" name="selected" value="1" />
		<button type="submit" class="btn btn-primary">Select</button>
	<?php endif; ?>
	<?=Form::close();?>
</div>
<?php
// echo "ID: \t\t" . $template -> id . "\n";
// echo "Name: \t\t" . $template -> name . "\n";
// echo "Packagename: \t" . $template -> packagename . "\n";
// echo "version: \t" . $template -> version . "\n";
// echo "description: \t" . $template -> description . "\n";
// echo "createdby: \t" . $template -> author . "\n";
// echo "published: \t" . $template -> published . "\n";
// echo "type: \t\t" . $template -> type . "\n";
// echo "Featured: \t" . $template -> featured . "\n";
// echo "Tags: \t\t";

// echo "\nPalettes:\n";
// foreach ($template->templatecolorpalettes->find_all() as $palette) {
	// echo "\n\tPalette {$palette->id}: \t";
	// foreach ($palette->templatecolor->order_by('importance')->find_all() as $color) {
		// echo "<span style=\"color:#" . $color -> hex . "\">" . $color -> name . " (" . $color -> importance . ")" . "</span>\n\t\t\t";
	// }
// }
?>

<?php
endforeach;
?>
</div>