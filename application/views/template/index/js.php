<script>
	$(function() {
		$('#templates').on('click', '.palette', function() {
			var $e = $(this),
			    $t = $e.closest('.template'),
			    $i = $t.find('img');
			$i.attr('src', "/assets/images/designs/" + $t.data('packagename') + "/" + $t.data('version') + "_" + $e.data('number') + "_small.png");
		})
	})
</script>