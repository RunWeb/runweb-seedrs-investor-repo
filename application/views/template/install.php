<a href="/templates">Templates</a> / <b>Install Template</b>
<form action="?" method="post" enctype="multipart/form-data">
<fieldset>
	<legend>Upload a new template</legend>
	<label><input type="file" name="template" /></label><button type="submit" class="btn btn-primary">Install Template</button>
</fieldset>	
</form>
