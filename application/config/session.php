<?php defined('SYSPATH') or die('No direct script access.');

if (IS_FRANCHISE) {
	return array(
	    'database' => array(
	        'name' => 'session',
	        'encrypted' => FALSE,
	        'lifetime' => 43200,
	        'group' => 'default',
	        'table' => 'franchise_sessions',
	        'columns' => array(
	            'session_id'  => 'session_id',
	            'last_active' => 'last_active',
	            'contents'    => 'contents'
	        ),
	        'gc' => 500,
	    ),
	);
} else {
	return array(
	    'database' => array(
	        'name' => 'session',
	        'encrypted' => FALSE,
	        'lifetime' => 43200,
	        'group' => 'default',
	        'table' => 'customer_sessions',
	        'columns' => array(
	            'session_id'  => 'session_id',
	            'last_active' => 'last_active',
	            'contents'    => 'contents'
	        ),
	        'gc' => 500,
	    ),
	);
}