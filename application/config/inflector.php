<?php defined('SYSPATH') or die('No direct script access.');

return array(

	'uncountable' => array(),

	'irregular' => array(
		'franchise' => 'franchises',
	),
);
