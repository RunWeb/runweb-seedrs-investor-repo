<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
	'driver'       => 'ORM',
	'hash_method'  => 'sha256',
	'hash_key'     => 'AYB2oqQuKb5s08dyr1tXDyb7a5aMl19N2k',
	'lifetime'     => 1209600,
	'session_type' => Session::$default,
	'session_key'  => 'auth_shop_user',
);
