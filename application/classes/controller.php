<?php defined('SYSPATH') or die('No direct script access.');

class Controller extends Kohana_Controller {

	protected $header = '';
	protected $footer = '';

	public function before() {
		parent::before();

	}

	public function headerfooter() {
		$this -> header = View::factory('common/header') -> set('title', (IS_FRANCHISE ? 'Franchise' : 'Customer') . ' Control Panel') -> set('user', Auth::instance() -> get_user());
		$this -> footer = View::factory('common/footer');
		return $this -> header;
	}

}
