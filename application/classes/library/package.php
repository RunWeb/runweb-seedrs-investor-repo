<?php defined('SYSPATH') or die('No direct script access.');

class Library_Package {
	// TODO: Move to config
	static public $temp_dir = '_temp';
	static public $package_dir = 'templatepackages';
	static function install($file) {
		// Upload files
		$filename = Upload::save($file, NULL, APPPATH . self::$package_dir . '/' . self::$temp_dir, '0777');
		if (empty($filename))
			return false; // TODO: Error objects
		// Extract uploaded file
		$folder = str_replace('.zip', '', $filename) . "/";
		if (!self::unzip($filename, $folder))
			return false; // TODO: Error objects
		// Load xml data
		$xml = $folder . 'package.xml';
		$data = self::decrypt_xml($xml);
		$template = ORM::factory('template');
		if (!$template->install($data)) 
			return false; // TODO: Error objects
		// Move files to package location
		if (!self::move_files($folder, $data)) 
			return false; // TODO: Error objects
		// Remove temp files
		
		// Successful install
		return true;
	}
	
	static function move_files ($folder, $data) {
		// Create folder
		$to_package_folder = APPPATH . self::$package_dir . '/' . $data['packagename'];
		$to_version_folder = $to_package_folder . '/' . str_replace('.', '-', $data['version']);
		echo Debug::vars($to_package_folder);
		Folders::mkdir($to_package_folder);
		// if (file_exists($to_version_folder)) {
			// // Files already exist, delete
			// Deletefolder::rrmdir ($to_version_folder);
		// }
		// Rename zipped files to version files
		Folders::copy($folder, $to_version_folder);
		return true;
	}

	static function unzip($filename, $folder) {
		$zip = new ZipArchive;
		$res = $zip -> open($filename);
		if ($res === TRUE) { 
			// TODO: Check folder rights
			$zip -> extractTo($folder);
			$zip -> close();
			return true;
		} else {
			return false;  // TODO: Error objects
		}
	}

	static function data_integrity($data) {
		
	}

	static function decrypt_xml($file) {
		if (file_exists($file)) {
			$package = new SimpleXMLElement(file_get_contents($file));
			// Load the attributes into the data array
			$attr = $package -> attributes();
			foreach ($attr as $a => $b) {
				$data[$a] = (string)$b;
			}
			// Load each data section into the main array
			foreach ($package as $element => $part) {
				switch($element) {
					// Single string valued items
					case 'author' :
					case 'type' :
					case 'description' :
					case 'name' :
						$data[$element] = (string)$part;
						break;
					// Multiple tags
					case 'tags' :
						$data[$element] = array();
						foreach ($part as $tag) {
							$data[$element][] = (string)$tag;
						}
						break;
					// Multiple palettes, each with multiple colors
					case 'palettes' :
						$data[$element] = array();
						$i = 0;
						foreach ($part as $palette) {
							$id = (int)$palette -> attributes() -> id;
							$data[$element][$i] = array('id' => $id, 'colors' => array());
							// Load palette colors
							foreach ($palette as $color) {
								$data[$element][$i]['colors'][] = array('hex' =>  (string)$color -> attributes() -> hex, 'name' => strtolower((string)$color -> attributes() -> name), 'importance' => (int)$color -> attributes() -> importance);
							}
							$i++;
						}
						break;
				}
			}
			return $data;
		} else {
			echo 'Package.xml not found'; // TODO: Error objects
		}
	}

}
