<?php defined('SYSPATH') or die('No direct script access.');

class Requires {
	static public function login($login = 'login', $fail = 'auth/login') {
		if (!Auth::instance() -> logged_in($login)) {
			Session::instance() -> set('before_login', Request::initial() -> uri());
			$response = Request::factory($fail) -> execute();
			echo $response -> status(200) -> send_headers() -> body();
			exit ;
		}
	}

}
