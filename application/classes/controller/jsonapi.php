<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Jsonapi extends Controller {
	private $model = false;

	public function action_index() {
		$this -> response -> body("true");
	}

	/* Domain search via fasthosts */

	public function action_domainavailable() {
		$domain_name = $this -> request -> param('id');
		if (!isset($domain_name) OR !is_string($domain_name) OR strlen($domain_name) <= 0)
			exit('false');

		$return['domain_name'] = $domain_name;

		$fasthost = Model::factory('fasthost');
		$is_domain_available = $fasthost -> is_domain_available($domain_name);
		if (!is_bool($is_domain_available) OR ($is_domain_available) !== true)
			$is_domain_available = false;

		$return['is_available'] = $is_domain_available;
		$this -> response -> body(json_encode($return));
	}

	public function action_domaincheck() {
		$domain_name = $this -> request -> param('id');
		if (!isset($domain_name) OR !is_string($domain_name) OR strlen($domain_name) <= 0)
			exit('false');

		$return['domain_name'] = $domain_name;
		$fasthost = Model::factory('fasthost');
		// Check domain availability
		$is_domain_available = $fasthost -> is_domain_available($domain_name);
		if (!is_bool($is_domain_available) OR ($is_domain_available) !== true)
			$is_domain_available = false;

		$return['is_available'] = $is_domain_available;
		// Get domain suggestions // Handled separately!
		// $suggestions = $fasthost -> domain_suggestions($domain_name, true);
		// $return['suggestions'] = $suggestions;
		$this -> response -> body(json_encode($return));
	}

	public function action_domainsuggestions() {
		$domain_name = $this -> request -> param('id');
		if (!isset($domain_name) OR !is_string($domain_name) OR strlen($domain_name) <= 0)
			exit('false');

		$return['domain_name'] = $domain_name;
		$fasthost = Model::factory('fasthost');
		// Get domain suggestions
		$suggestions = $fasthost -> domain_suggestions($domain_name, true);
		$return['suggestions'] = $suggestions;
		$this -> response -> body(json_encode($return));
	}

	/* Address lookup via simply postal address finder */

	public function action_address() {
		$address_id = $this -> request -> param('id');
		if (!isset($address_id) OR !is_string($address_id) OR strlen($address_id) <= 0)
			exit('false');

		$return['address_id'] = $address_id;

		$PAF = Model::factory('paf');

		$data = $PAF -> get_address_from_id($address_id);

		if (!is_array($data) OR count($data) <= 0)
			$data = false;

		$return['address'] = $data;
		$this -> response -> body(json_encode($return));
	}

	public function action_addresses() {
		$postcode = $this -> request -> param('id');
		if (!isset($postcode) OR !is_string($postcode) OR strlen($postcode) <= 0)
			exit('false');

		$return['postcode'] = $postcode;

		$PAF = Model::factory('paf');

		$data = $PAF -> get_addresses_from_postcode($postcode);

		if (!is_array($data) OR count($data) <= 0)
			$data = false;
		$return['addresses'] = $data;
		$this -> response -> body(json_encode($return));
	}

} // End Jsonapi
