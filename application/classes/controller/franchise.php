<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Franchise extends Controller {

	public function before() {
		parent::before();
		//Session::instance('database') -> set('test', 'hello') -> write();
		Requires::login('franchise_login');
	}

	public function action_index() {
		$this -> headerfooter();
		$this -> response -> body($this -> header . View::factory('franchise/cpfront') . $this -> footer);
	}

	public function action_customers() {
		$this -> headerfooter() -> set('title', 'Customer List');
		$user = Auth::instance() -> get_user();
		$franchise = $user -> franchise;
		$customers = $franchise -> customers -> find_all() -> as_array();
		$this -> response -> body($this->header . View::factory('franchise/customers')->bind('user', $user) -> bind('franchise', $franchise) -> bind('customers', $customers) . $this->footer);
	}
	

} // End Franchise
