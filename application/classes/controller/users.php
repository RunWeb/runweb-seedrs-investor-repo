<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Users extends Controller {

	public function before() {
		parent::before();
		Requires::login('runweb_login', 'permission');
	}

	public function action_index() {
		$this -> headerfooter() -> set('title', 'Users');
		$franchise = ORM::factory('franchise', $this->request->param('franchise'));
		$users = $franchise -> franchise_users -> find_all() -> as_array();
		$content = View::factory('user/list') -> bind('users', $users) -> bind('franchise', $franchise);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

	public function action_single() {
		$this -> headerfooter() -> set('title', 'User');
		$franchise = ORM::factory('franchise', $this->request->param('franchise'));
		$user = $franchise -> franchise_users -> where('id' , '=', $this -> request -> param('user')) -> find();
		$content = View::factory('user/single') -> set('page', 'franchise/' . $franchise -> id . '/user/' . $user -> id)-> bind('account', $user) -> bind('franchise', $franchise);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}
	
	public function action_edit() {
		// Load the page header/footer
		$this -> headerfooter() -> set('title', 'Edit User');
		
		$franchise = ORM::factory('franchise', $this->request->param('franchise'));
		$user = $franchise -> franchise_users -> where('id' , '=', $this -> request -> param('user')) -> find();
		
		// Get input vars
		$post = $this -> request -> post();
		// Update account details
		if (!empty($post)) {
			unset ($post['id']);
			$user -> values($post) -> save();
			$this -> request -> redirect('franchise/' . $franchise -> id . '/user/' . $user -> id);
		}
		
		// Show account details form / display
		$view = View::factory('account/details/edit') -> set('page', 'franchise/' . $franchise -> id . '/user/' . $user -> id)-> bind('account', $user) -> bind('franchise', $franchise);
		$view -> bind('account', $user);
		$this -> response -> body($this -> header . $view . $this -> footer);
	}

} // End Users
