<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Templates extends Controller {
	private $session;
	public function before() {
		$this -> session = Session::instance('database');
		Requires::login('franchise_login');
	}

	public function action_bill() {
		try {

			$customer = ORM::factory('customer', 1);
			//$bill = ORM::factory('bill', 1);
			// $bill -> customer_id = $customer -> id;
			// $bill -> date = DB::expr('CURDATE()');
			// $bill -> vat = '20.0';
			// $bill -> status = 0 ;
			// $bill -> save();
			$this -> response -> body(Debug::vars($customer -> bills -> find_all() -> as_array()));
		} catch (ORM_Validation_Exception $e) {
			$errors = $e -> errors();
			$this -> response -> body(Debug::vars($errors));
		}
	}

	public function action_index() {
		$id = (int)$this -> request -> post('id');
		$selected = (int)$this -> request -> post('selected');
		$franchise = Auth::instance() -> get_user() -> franchise;
		if ($id > 0) {
			$template = ORM::factory('template', $id);
			if ($selected)
				$template -> add('franchises', $franchise);
			else
				$template -> remove('franchises', $franchise);
			$this -> request -> redirect('templates');
		}
		$this -> headerfooter() -> set('title', 'Templates');
		$templates = ORM::factory('template') -> where('id', '>', 0) -> find_all();
		//$red = ORM::factory('templatecolor') -> where('name', '=', "red") -> find_all();
		$shopfront = View::factory('template/index') -> set('templates', $templates) -> set('franchise', $franchise);
		// -> set('red', $red);
		if ($message = $this -> session -> get_once('statusmessage')) {
			$shopfront -> set('message', $message);
		}
		$this -> footer -> set('inline_scripts', View::factory('template/index/js'));
		$this -> response -> body($this -> header . $shopfront . $this -> footer);
	}

	public function action_install() {
		$this -> headerfooter() -> set('title', 'Install Template');
		$data = array();
		$files = Validation::factory($_FILES);
		if (!empty($files['template'])) {
			Package::install($files['template']);
			$this -> session -> set('statusmessage', 'Install successfull!');
			$this -> request -> redirect('templates/index');
		}
		$this -> response -> body($this -> header . View::factory('template/install') . $this -> footer);
	}

} // End Templates
