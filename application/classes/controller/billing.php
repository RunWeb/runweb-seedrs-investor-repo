<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Billing extends Controller {

	public function before() {
		parent::before();
		//Session::instance('database') -> set('test', 'hello') -> write();
		Requires::login('login');
	}

	public function action_index() {
		$this -> headerfooter() -> set('title', 'Billing');
		$user = ORM::factory('customer', Auth::instance() -> get_user() -> id);
		$bills = $user -> bills -> find_all() -> as_array();
		$this -> response -> body($this -> header . View::factory('billing/list') -> bind('user', $user) -> bind('bills', $bills) . $this -> footer);
	}

} // End Account
