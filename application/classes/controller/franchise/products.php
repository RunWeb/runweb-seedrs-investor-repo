<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Franchise_Products extends Controller {

	public function before() {
		parent::before();
		Requires::login('runweb_login', 'permission');
	}

	public function action_enable() {
		$franchise = ORM::factory('franchise', $this -> request -> param('franchise'));
		$enabled = $this -> request -> post('enabled');
		$id = $this -> request -> post('id');
		ORM::factory('franchise_product', $id) -> set('enabled', !$enabled) -> save();
		$this -> request -> redirect('franchise/' . $franchise -> id . '/products');
	}
	
	public function action_edit() {
		/*
		 * Headers
		 */
		
		$this -> headerfooter() -> set('title', 'Edit Franchise Product');
		$franchise = ORM::factory('franchise', $this -> request -> param('franchise'));
		$product = $franchise -> franchise_products -> where('id', '=', $this -> request -> param('product')) -> find();
		
		/*
		 * Post
		 */
		
		if ($this -> request -> post('id') == $product -> id && $product -> id > 0) {
			$product -> values ($this -> request -> post()) -> save();
			$this -> request -> redirect('franchise/' . $franchise -> id . '/products/');
		}
		
		
		/*
		 * Content
		 */
		
		$content = View::factory('franchise/products/edit') -> bind('franchise', $franchise) -> bind('product', $product);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}
	
	public function action_index() {
		$this -> headerfooter() -> set('title', 'Franchise Products');
		$franchise = ORM::factory('franchise', $this -> request -> param('franchise'));
		$products = $franchise -> franchise_products -> find_all() -> as_array();
		$content = View::factory('franchise/products') -> bind('franchise', $franchise) -> bind('products', $products);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

} // End Franchise_Products
