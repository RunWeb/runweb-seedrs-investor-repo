<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Franchise_Auth extends Controller_Common_Auth {

	protected $_required_role = 'franchise_login';
	
	public function action_user() {
		try {
			ORM::factory('user') -> create_user(array('username' => 'gstar', 'email' => 'info@runweb.co.uk', 'password_confirm' => 'johnlewis', 'password' => 'johnlewis', 'franchise_id' => 1), array('username', 'email', 'password', 'franchise_id'));
			$this -> response -> body("Logged in");
		} catch (ORM_Validation_Exception $e) {
			$this -> response -> body($e -> getMessage());
		}
	}
} // End Auth
