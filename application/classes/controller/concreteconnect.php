<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Concreteconnect extends Controller {
	public function action_index() {
		$path = $this -> request -> param('path');

		if ($path == 'view') {
			$this -> action_view();
		} else {
			$post = $this -> request -> post();
			$get = array();
			$get_raw = $_SERVER['QUERY_STRING'];
			parse_str($get_raw, $get);
			
			switch($path) {
			//switch (2) {
				case 'marketplace/connect/-/validate' :
				case "marketplace/connect/-/enable_free_license" :
					$this -> response -> body('');
					break;
				case 'marketplace/themes/-/get_remote_item_sets' :
					$out = array();
					$tags = ORM::factory('template_tag') -> find_all() -> as_array();
					foreach ($tags as $tag) {
						$out[] = array('marketplaceItemSetID' => $tag -> id, 'marketplaceItemSetName' => $tag -> tag);
					}
					$this -> response -> body(json_encode($out));
					break;
				case "marketplace/themes/-/get_remote_list" :
					$out = array();
					$sort = isset($get['sort']) ? $get['sort'] : NULL;
					$version = isset($get['version']) ? $get['version'] : '5.5.2';
					$itemsPerPage = isset($get['itemsPerPage']) ? (int) $get['itemsPerPage'] : 9;
					$csToken = isset($get['csToken']) ? $get['csToken'] : NULL;
					$includeInstalledItems = isset($get['includeInstalledItems']) ? (bool) $get['includeInstalledItems'] : FALSE;
					$installedPackages = isset($get['installedPackages']) ? $get['installedPackages'] : array();
					$page = isset($get['ccm_paging_p']) ? $get['ccm_paging_p'] : array();
					$set = isset($get['set']) ? (int) $get['set'] : 0;
					$keywords = isset($get['keywords']) ? $get['keywords'] : NULL;
					$is_compatible = isset($get['is_compatible']) ? $get['is_compatible'] : FALSE;
					// TODO: Every search term
					$templates = ORM::factory('template') -> find_all() -> as_array();
					$out['total'] = (int) ORM::factory('template') -> count_all();
					$out['items'] = array();
					foreach ($templates as $template) {
						$out['items'][] = array(
							'url' => 'http://franchise.runweb2.co.uk/templates/package/' . $template -> packagename,
							'mpID' => $template -> id,
							'name' => $template -> name,
							'description' => $template -> description,
							'listicon' => 'http://franchise.runweb2.co.uk/assets/images/designs/'.$template -> packagename.'/'.str_replace('.','-',$template -> version).'_1_icon.png',
							'icon' => 'http://franchise.runweb2.co.uk/assets/images/designs/'.$template -> packagename.'/'.str_replace('.','-',$template -> version).'_1_icon.png',
							'largethumbnail ' => array(
								'height' => 400,
								'width'  => 400,
								'src'	 => 'http://franchise.runweb2.co.uk/assets/images/designs/'.$template -> packagename.'/'.str_replace('.','-',$template -> version).'_1_large.png',
							),
							'handle' => $template -> packagename,
							'pkgVersion' => $template -> version,
							'price' => '',
							'cId' => $template -> id,
							'islicensed' => true,
						);
					}
					$this -> response -> body(json_encode($out));
					break;
				case "marketplace/connect/-/get_item_information":
					
					$mpID = isset($get['mpID']) ? $get['mpID'] : NULL;
					$template = ORM::factory('template', $mpID);
					$out = array(
						'bodyContent' => $template -> description,
						'cID' => $template -> id,
						'description' => $template -> description,
						'file' => 'http://franchise.runweb2.co.uk/package/'.$template -> packagename.'/'.str_replace('.','-',$template -> version).'/1.zip',
						'fivePackProductBlockID' => NULL,
						'handle' =>  $template -> packagename,
						'icon' => 'http://franchise.runweb2.co.uk/assets/images/designs/'.$template -> packagename.'/'.str_replace('.','-',$template -> version).'_1_icon.png',
						'islicensed' => TRUE,
						'mpID' => $template -> id,
						'mpType' => 'theme',
						'name' => $template -> name,
						'pkgVersion' => $template -> version,
						'price' => '0.00',
						'productBlockID' => $template -> id,
						'rating' => '',
						'reviewBody' => '',
						'reviewsURL' => '',
						'screenshots' => array(
								array(
									'height' => 400,
									'width'  => 400,
									'src'	 => 'http://franchise.runweb2.co.uk/assets/images/designs/'.$template -> packagename.'/'.str_replace('.','-',$template -> version).'_1_large.png',
								),
							),
						'siteLatestAvailableVersion' => $template -> version,
						'totalRatings' => '0',
						'url' => '',	// URL to marketplace page
						'versionHistory' => '',
					);
					$this -> response -> body(json_encode($out));
					break; 
				default :
					$headers = $this -> request -> headers();
					$cc = ORM::factory('concreteconnect');
					$cc -> path = serialize($path);
					$cc -> post = serialize(array('post' => $post, 'get' => $get, 'get_raw' => $get_raw));
					$cc -> headers = serialize($headers);
					$cc -> save();
					$this -> response -> body(str_replace('www.concrete5.org', 'franchise.runweb2.co.uk/concreteconnect', file_get_contents('http://www.concrete5.org/' . $path . '?' . $get_raw)));
			}
		}
	}

	public function action_view() {
		$cc = ORM::factory('concreteconnect') -> order_by('id', 'DESC') -> find_all() -> as_array();
		$out = '';
		foreach ($cc as $part) {
			$path = unserialize($part -> path);
			$post = unserialize($part -> post);
			$headers = unserialize($part -> headers);
			$out .= '<hr /><h2>' . $part -> id . '</h2>';
			$link = 'http://www.concrete5.org/' . $path . '?' . $post['get_raw'];
			$out .= '<h3>Link</h3>' . '<a href="' . $link . '" target="_blank">' . $link . '</a>';
			$out .= '<h3>Path</h3>' . Debug::vars($path);
			$out .= '<h3>Post</h3>' . Debug::vars($post);
			$out .= '<h3>Headers</h3>' . Debug::vars($headers);
		}
		$this -> response -> body($out);
	}

}
