<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Products extends Controller {

	public function before() {
		parent::before();
		//Session::instance('database') -> set('test', 'hello') -> write();
		Requires::login('franchise_login');
	}

	public function action_index() {
		$this -> headerfooter() -> set('title', 'Products');
		$products = ORM::factory('product') -> find_all() -> as_array();
		$this -> response -> body($this -> header . View::factory('products/index') -> bind('products', $products) . $this -> footer);
	}

	public function action_disable() {
		$id = $this -> request -> post('id');
		$enabled = $this -> request -> post('enabled');
		$product = ORM::factory('product', $id) -> set('enabled', $enabled) -> save();
		$this -> request -> redirect('/products');
	}

	public function action_edit() {
		$this -> headerfooter() -> set('title', 'Products');
		$id = $this -> request -> param('id');
		if ($this -> request -> post('id') != 0) {
			$id = $this -> request -> post('id');
			$product = ORM::factory('product', $id);
			$product -> values($this -> request -> post()) -> save();
			$this -> request -> redirect('/products');
		} else {
			$product = ORM::factory('product', $id);
			$this -> response -> body($this -> header . View::factory('products/single') -> bind('product', $product) -> set('add', false) . $this -> footer);
		}
	}
	
	public function action_add() {
		$this -> headerfooter() -> set('title', 'Products');
		
		if ($this -> request -> post('id')) {
			$product = ORM::factory('product') -> values($this -> request -> post()) -> save();
			$this -> request -> redirect('/products');
		}
		
		$product = new stdClass;
		$product -> id = 'new';
		$product -> baseprice = 0;
		$product -> description = '';
		$product -> name = '';
		$product -> enabled = 1; 
		$this -> response -> body($this -> header . View::factory('products/single') -> bind('product', $product) -> set ('add', true) . $this -> footer);
	}

} // End Franchise
