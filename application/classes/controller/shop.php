<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Shop extends Controller {

	protected $header = '';
	protected $footer = '';

	public function before() {
		parent::before();
		Requires::login('login');
	}

	public function headerfooter() {
		$this -> header = View::factory('common/header') -> set('title', 'Customer Control Panel');
		$this -> footer = View::factory('common/footer');
	}

	public function action_index() {
		$this -> headerfooter();
		$this -> response -> body($this -> header . View::factory('customer/cpfront') . $this -> footer);
	}

	public function action_dumptables() {
		$output = '<div><h1>Tables';
		foreach (array('customer', 'bill', 'bill_line', 'customer_log_action', 'customer_log', 'customer_product', 'franchise_available_product', 'franchise_log_action', 'franchise_log', 'franchise_product', 'franchise_template', 'franchise_user', 'template_color', 'template_palette', 'template_tag', 'franchise', 'precustomer', 'product', 'template') as $table) {
			$data = ORM::factory($table) -> list_columns();
			foreach ($data as $k => $v) {
				foreach ($v as $key => $del) {
					if ($key !== 'type' && $key !== 'is_nullable')
						unset($data[$k][$key]);
				}
			}
			$output .= "</div>\n" . str_repeat("-", 20) . " $table " . str_repeat("-", 20) . "\n <div>protected \$_table_columns = " . var_export($data, 1) . ";";
		}
		$this -> response -> body('<pre>' . $output);
		// . "Shop <a href=\"/auth/logout\">Log out</a>". View::factory('profiler/stats'));
	}

	public function action_domain() {
		$id = $this -> request -> param('id');

		$sessionid = Session::instance('database') -> id();
		$precustomer = ORM::factory('precustomer') -> where('session', '=', $sessionid) -> find();
		$domainpage = View::factory('shop/domain') -> set('customer', $precustomer);
		$this -> response -> body($domainpage);
	}

	public function action_data() {
		$data = $this -> request -> post();
		$precustomer = ORM::factory('precustomer') -> where('session', '=', Session::instance('database') -> id()) -> find();
		$precustomer -> values($data);
		$precustomer -> date = DB::expr('now()');
		$precustomer -> session = Session::instance('database') -> id();
		$precustomer -> save();
		if ($this -> request -> post('redirect')) {
			$this -> request -> redirect("shop");
		} else {
			echo "true";
		}
	}

	public function action_test() {
		Session::instance('database') -> set('test', 'hello') -> write();
		$this -> response -> body("<a href='/auth/logout'>Logged in</a> " . Session::instance('database') -> id());
	}

	public function action_will() {
		$emails = Model::factory('email');
		$emails -> annoyWill();
	}

	public function action_create() {
		// Set config
		$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl') -> setUsername('support@runweb.co.uk') -> setPassword('yHZV3t8diaawYU3vC');
		$sessionid = Session::instance('database') -> id();
		$precustomer = ORM::factory('precustomer') -> where('session', '=', $sessionid) -> find();

		if (!$sessionid)
			die('Empty session');
		if (!$precustomer -> id)
			die('No user in db');
		if (!$precustomer -> domain)
			die('No domain');
		if (!$precustomer -> fullname)
			die('No name');

		$do = array('a' => false, 'b' => false, 'c' => false, 'd' => false, 'e' => true, 'f' => true, );

		$out = '';

		// $shopfront = View::factory('shop/front') -> set('customer', $precustomer);
		// This is the function that initialises everything - i.e. thanks page

		// 8a. Send email to client
		$out .= 'Email step: ';
		if ($do['a']) {
			$mailer = Swift_Mailer::newInstance($transport);
			$message = Swift_Message::newInstance() -> setSubject("Building site") -> setTo(array('Info@runweb.co.uk' => 'info')) -> setFrom(array('support@runweb.co.uk' => 'RunWeb Support')) -> setBody('Thanks for the payment. Wait an hour or so. Any help, email us. Password will be mailed when your site is ready.');
			//-> addPart('', 'text/html');
			$mailer -> send($message);
			$out .= 'complete';
		} else {
			$out .= 'skipped';
		}
		// 8b. Create UID
		$customer = Model::factory('customer');
		$customer_id = $customer -> createUserId($precustomer -> domain);
		$customer_password = $customer -> createPassword();
		$out .= '<br />Customer id: ' . $customer_id;
		$out .= '<br />Customer password: ' . $customer_password;

		// Fasthosts
		if ($do['c'] || $do['d']) {
			$fasthosts = Model::factory('fasthost');
		}

		// 8c. Fasthosts create new customer
		$out .= '<br />Fasthosts create new customer: ';
		if ($do['c']) {
			$name = explode(' ', $precustomer -> fullname);
			$fasthosts_customer = new stdClass();
			$contact = new stdClass();
			$contact -> FirstName = $name[0];
			$contact -> LastName = $name[1];
			$contact -> Address1 = $precustomer -> houseno;
			$contact -> Address2 = $precustomer -> street;
			$contact -> City = $precustomer -> town;
			$contact -> Country = new stdClass();
			$contact -> Country -> Code = 'GB';
			$contact -> Region = $precustomer -> county;
			$contact -> AreaCode = $precustomer -> postcode;
			$contact -> UKRegistrantType = "IND";
			$tel = $precustomer -> telephone;
			$contact -> PhoneNumber = '+44-' . substr($tel, 1, 4) . '-' . substr($tel, 5);
			$contact -> Email = $precustomer -> email;
			$customer -> Username = $customer_id;
			$customer -> Password = $customer_password;
			$customer -> Contact = $contact;
			echo Debug::vars($customer);
			$fasthosts_customer_id = $fasthosts -> createCustomer($customer);
			$out .= 'complete, id ' . $fasthosts_customer_id;
		} else {
			$out .= 'skipped';
			$fasthosts_customer_id = 'UK1507568291';
		}

		// 8d. Fasthosts domain registration

		$out .= '<br />Fasthosts domain reg: ';
		if ($do['d']) {
			if ($fasthosts -> is_domain_available($precustomer -> domain)) {
				$order = $fasthosts -> buydomain($fasthosts_customer_id, $precustomer -> domain);
				echo Debug::vars($order);
			}
			$out .= 'complete';
		} else {
			$out .= 'skipped';
		}

		// 8d.2 Check the order is fully completed (cron job?) - can be rejected

		// TODO

		// 8e. Plesk create user

		if ($do['e']) {
			// Customer object
			$precustomer = ORM::factory('precustomer', $this -> request -> post('id'));
			// Create the customer
			$plesk = ORM::factory('pleskaction');
			$plesk -> program = "customer";
			$cmd = $plesk -> arguments = implode(" ", array('--create', "$customer_id", // Set the username as the domain
			'-name', "\"{$precustomer->fullname}\"", '-passwd', "\"$customer_password\"", '-email', "\"{$precustomer->email}\"", '-address', "\"{$precustomer->houseno}, {$precustomer->street}\"", '-city', "\"{$precustomer->town}\"", '-state', "\"{$precustomer->county}\"", '-zip', "\"{$precustomer->postcode}\"", '-country', "GB", '-phone', "\"{$precustomer->telephone}\"", ));
			$plesk -> done = 0;
			$plesk -> date = DB::expr('now()');
			$plesk -> save();

			$out .= '<br />Sent off create customer <code>' . $cmd . '</code>';
		} else {
			$out .= '<br />Skipped create customer';
		}

		// 8f. Plesk subscription
		if ($do['f']) {
			$plesk = ORM::factory('pleskaction');
			$plesk -> program = "subscription";
			$username = substr(str_replace('.', '', $precustomer -> domain), 0, 17);
			// Limited to 17 length
			$cmd = $plesk -> arguments = implode(" ", array('--create', "{$precustomer->domain}", '-owner', "$customer_id", '-service-plan', 'Joomla', '-login', "{$username}", // Set the username as the domain
			'-passwd', "\"$customer_password\"", '-ip', "88.208.228.140", ));
			$plesk -> done = 0;
			$plesk -> date = DB::expr('now()');
			$plesk -> save();
			$out .= '<br />Sent off create domain <code>' . $cmd . '</code>';
		} else {
			$out .= '<br />Skipped create customer lpHNpkJWEHP79XYb!';
		}

		// 8g. Joomla install
		// Choose table prefix

		// 8h. Joomla template

		// 8i. Joomla user account setup

		// 8j. Test joomla install

		// 8k. Email creation

		// 8l. Creating email login details

		// 8m. Send emails to client and administrator - site is up

		$this -> response -> body($out);
	}

} // End Shop
