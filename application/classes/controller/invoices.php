<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Invoices extends Controller {

	public function before() {
		parent::before();
		Requires::login('runweb_login', 'permission');
	}

	public function action_index() {
		$this -> headerfooter() -> set('title', 'Invoices');
		$franchise = ORM::factory('franchise', $this->request->param('franchise'));
		$invoices = $franchise -> invoices -> find_all() -> as_array();
		$content = View::factory('invoice/list') -> bind('invoices', $invoices) -> bind('franchise', $franchise);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

	public function action_single() {
		$this -> headerfooter() -> set('title', 'Invoice');
		$franchise = ORM::factory('franchise', $this->request->param('franchise'));
		$invoice = $franchise -> invoices -> where('id' , '=', $this -> request -> param('invoice')) -> find();
		$content = View::factory('invoice/single') -> bind('invoice', $invoice) -> bind('franchise', $franchise);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

} // End Invoices
