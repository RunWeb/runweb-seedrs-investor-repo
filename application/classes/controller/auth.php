<?php defined('SYSPATH') or die('No direct script access.');

if (IS_FRANCHISE) {
	class Controller_Auth extends Controller_Franchise_Auth {} 
}
if (IS_SHOP) {
	class Controller_Auth extends Controller_Customer_Auth {} 
}
