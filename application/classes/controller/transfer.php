<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Transfer extends Controller {

	public function action_index() {
		$this -> response -> body(View::factory('transfer/index'));
	}

	public function action_move() {
		$move = $this->request->post('domain');
		$move = 'runweb2.co.uk';
		// if (empty($move)) {
			// url::redirect('/kohana/transfer');
		// }
		$fasthost = Model::factory('fasthost');
		$domain = $fasthost->getdomain($move);
		$nameservers = array();
		$nameservers[0] = new stdClass;
		$nameservers[0]->Name = 'ns1.runweb.co.uk';
		$nameservers[0]->IP = '';
		$nameservers[1] = new stdClass;
		$nameservers[1]->Name = 'ns2.runweb.co.uk';
		$nameservers[1]->IP = '';
		$domain->NameServers = $nameservers;
		$fasthost->UpdateDomain($domain);
		
		$view = View::factory('transfer/do')
			-> bind('url', $move)
			-> bind('fasthost', $fasthost)
			-> bind('domain', $domain)
			-> bind('nameservers', $nameservers);
		$this -> response -> body($view);
	}

} // End Welcome
