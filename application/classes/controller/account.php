<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Account extends Controller {

	public function before() {
		parent::before();
		//Session::instance('database') -> set('test', 'hello') -> write();
		Requires::login(IS_FRANCHISE ? 'franchise_login' : 'login');
	}

	public function action_index() {
		$this -> action_details();
	}

	public function action_details() {
		// Load the page header/footer
		$this -> headerfooter() -> set('title', 'Account Details');
		// Get input vars
		$edit = ($this -> request -> param('id') == 'edit');
		$post = $this -> request -> post();
		// Load user
		$account = Auth::instance() -> get_user();
		// Update account details
		if ($edit && !empty($post)) {
			$account -> values($post) -> save();
			$this -> request -> redirect("account/details");
		}
		
		// Show account details form / display
		if ($edit)
			$view = View::factory('account/details/edit');
		else
			$view = View::factory('account/details');
		$view -> bind('account', $account);
		$this -> response -> body($this -> header . $view . $this -> footer);
	}

	public function action_createuser() {
		try {
			$user = ORM::factory('user') -> create_user(array('username' => 'gstar', 'password' => 'hustletronic2012', 'password_confirm' => 'hustletronic2012', 'email' => 'guser@runweb.co.uk', 'franchise_id' => 1), array('username', 'password', 'email', 'franchise_id', ));
		} catch (ORM_Validation_Exception $e) {
			echo Debug::vars($e -> getMessage());
		}
	}

	public function action_addtag() {
		$user = ORM::factory('user', array('username' => 'gstar')) -> add('roles', ORM::factory('role', array('name' => 'login')));
	}

} // End Account
