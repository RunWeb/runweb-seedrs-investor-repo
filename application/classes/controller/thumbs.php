<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Thumbs extends Controller {
	public function action_index() {
		$packagename = $this -> request -> param('packagename');
		$version = $this -> request -> param('version');
		$palette = $this -> request -> param('palette');
		$size = $this -> request -> param('size');
		// TODO: Find alternatives if they don't exist
		$packagepath = APPPATH . 'templatepackages' . DIRECTORY_SEPARATOR . $packagename;
		$versionpath = $packagepath . DIRECTORY_SEPARATOR . $version;
		if ($size == 'icon') {
			$pngpath = $versionpath . DIRECTORY_SEPARATOR . 'icon.png';
		}
		else {
			$pngpath = $versionpath . DIRECTORY_SEPARATOR . 'thumbnail' . '_' . $palette . ($size ? '_' . $size : '') . '.png';
		}
		if (!file_exists($pngpath))
			return false;
		else {
			$designpath = 'assets/images/designs/' . $packagename . '/';
			$output = $designpath . $version . '_' . $palette . ($size ? '_' . $size : '') . '.png';
			Folders::mkdir(DOCROOT . $designpath);
			copy($pngpath, DOCROOT . $output);
			if (file_exists($output)) {
				$this -> request -> redirect($output);
			} else {
				echo 'Image not found!';
			}
		}
	}

} // End Templates
