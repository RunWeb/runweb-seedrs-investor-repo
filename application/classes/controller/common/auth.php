<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Common_Auth extends Controller {

	public function before() {
		parent::before();
	}

	public function action_index() {
		$this -> action_login();
	}

	public function action_user() {

	}

	public function action_login() {
		$username = $this -> request -> post('username');
		$password = $this -> request -> post('password');
		$remember = (bool)$this -> request -> post('remember');
		$token = $this -> request -> post('csrf');
		$auth = Auth::instance();
		// Attempt auto login
		$auth -> auto_login();
		// Attempt login
		if (!Security::check($token)) 
			$this -> response -> body('CSRF Failed');
		if (!$auth -> logged_in($this -> _required_role) && Security::check($token))
			$auth -> login($username, $password, $remember);
		
		if (!$auth -> logged_in($this -> _required_role)) {
			$view = View::factory('auth/login');
			if (!empty($username) && !empty($password)) {
				$view -> set('message', 'Invalid login') -> set('username', $username) -> set('password', '') -> set('remember', $remember);
			}
			//echo Session::instance('database') -> get('test');
			$this -> response -> body($view->set('role', $this -> _required_role)); // . View::factory('profiler/stats')
		} else {
			// Redirect to intial request
			$this -> request -> redirect(Session::instance() -> get('before_login'));
		}
		
	}

	public function action_logout() {
		Auth::instance() -> logout();
		$this -> request -> redirect('/');
	}

} // End Auth
