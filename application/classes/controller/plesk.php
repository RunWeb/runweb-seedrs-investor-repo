<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Plesk extends Controller {
	public function before() {
		parent::before();
		if (!Auth::instance() -> logged_in('runweb_login')) {
			$request = Request::factory('auth/login');
		}
	}

	public function action_index() {
		$this -> action_customer();
	}

	public function action_customer() {
		$id = $this -> request -> param('id');
		$this -> response -> body(View::factory('plesk/forms/customer') 
			-> set('id', $id)
			-> set('errors', array(
				'username' => array(
					'message' => 'Username field must not be empty'
				),
				'name' => array(
					'message' => 'Name field must not be empty'
				),
				'email' => array(
					'message' => 'Email field must not be empty'
				),
				'password' => array(
					'message' => 'Can\'t be rubbish'
				),
				'passwordconfirm' => array(
					'message' => 'Must match'
				),
				'phone' => array(
					'message' => 'Must be a real number'
				),
			))
			-> set('values', array(
				'username' => "",
				'name' => "",
				'email' => "",
				'password' => "",
				'passwordconfirm' => "",
			))
		);
	}

	public function action_add() {
		$id = $this->request->post('id');
		$confirm = $this->request->post('confirm');
		$type = $this->request->post('type');
		// Customer object 
		$precustomer = ORM::factory('precustomer', $this->request->post('id'));
		// Create the customer
		$plesk = ORM::factory('pleskaction');
		$plesk->program = "customer";
		$plesk->arguments = implode(" ", array(
			'--create', "testaccount", // Set the username as the domain
			'-name', "\"{$precustomer->fullname}\"",
			'-passwd', "asd1234",
			'-email', "\"{$precustomer->email}\"",
			'-address', "\"{$precustomer->houseno}, {$precustomer->street}\"",
			'-city', "\"{$precustomer->town}\"",
			'-state', "\"{$precustomer->county}\"",
			'-zip', "\"{$precustomer->postcode}\"",
			'-country', "GB",
			'-phone', "\"{$precustomer->telephone}\"",
			));
		$plesk->done = 0;
		$plesk->date = DB::expr('now()');
		$plesk->save();
		// Create the subscription
		$plesk = ORM::factory('pleskaction');
		$plesk->program = "subscription";
		$username = substr(str_replace('.', '', $precustomer->domain), 0, 17); // Limited to 17 length
		$plesk->arguments = implode(" ", array(
			'--create', "{$precustomer->domain}", 
			'-owner', "testaccount",
			'-service-plan', $type,
			'-login', "{$username}",// Set the username as the domain
			'-passwd', "asd1234",
			'-ip', "88.208.228.140",
			));
		$plesk->done = 0;
		$plesk->date = DB::expr('now()');
		$plesk->save();
	}

} // End Welcome
