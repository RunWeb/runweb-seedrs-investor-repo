<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Customers extends Controller {

	public function before() {
		parent::before();
		Requires::login('runweb_login', 'permission');
	}

	public function action_index() {
		$this -> headerfooter() -> set('title', 'Customers');
		$franchise = ORM::factory('franchise', $this -> request -> param('franchise'));
		$customers = $franchise -> customers -> find_all() -> as_array();
		$content = View::factory('customer/list') -> bind('users', $customers) -> bind('franchise', $franchise);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

	public function action_single() {
		$this -> headerfooter() -> set('title', 'Customer');
		$franchise = ORM::factory('franchise', $this -> request -> param('franchise'));
		$customer = $franchise -> customers -> where('id', '=', $this -> request -> param('customer')) -> find();
		$content = View::factory('customer/single') -> bind('customer', $customer) -> bind('franchise', $franchise);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

} // End Customers
