<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Permission extends Controller {

	public function action_index() {
		$this -> headerfooter() -> set('title', 'Error!');
		$content = View::factory('errors/permission');
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

} // End Franchises
