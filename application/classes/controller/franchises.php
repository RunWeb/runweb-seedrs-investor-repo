<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Franchises extends Controller {

	public function before() {
		parent::before();
		Requires::login('runweb_login', 'permission');
	}

	public function action_index() {
		$this -> headerfooter() -> set('title', 'Franchises');
		$franchises = ORM::factory('franchise') -> find_all() -> as_array();
		$content = View::factory('franchise/list') -> bind('franchises', $franchises);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

	public function action_single() {
		$this -> headerfooter() -> set('title', 'Franchise');
		$franchise = ORM::factory('franchise', $this -> request -> param('id'));
		$content = View::factory('franchise/single') -> bind('franchise', $franchise);
		$this -> response -> body($this -> header . $content . $this -> footer);
	}

	public function action_edit() {
		/*
		 * Request
		 */

		$this -> headerfooter() -> set('title', 'Edit Franchise');
		$franchise_id = (int)$this -> request -> param('id');
		$message = '';

		/*
		 * Post actions
		 */
		try {
			$post = $this -> request -> post('action') == 'edit';
			if ($post) {
				$id = (int)$this -> request -> post('id');
				if ($id !== $franchise_id) {
					throw new Exception("IDs don't match!" . $id . ':' . $franchise_id);
				}
				$franchise = ORM::factory('franchise', $id);
				$franchise -> values($this -> request -> post());
				$franchise -> save();
				$this -> request -> redirect('franchise/' . $id);
			}
		} catch (Exception $e) {
			$message = $e;
		}

		/*
		 * Display
		 */

		$franchise = ORM::factory('franchise', $franchise_id);
		$this -> response -> body($this -> header . View::factory('franchise/edit') -> bind('franchise', $franchise) -> set('message', $message) . $this -> footer);
	}

} // End Franchises
