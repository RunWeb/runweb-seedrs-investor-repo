<?php defined('SYSPATH') or die('No direct access allowed.');

class Filter {
	static function postcode($postcode = '') {
		return strtoupper(str_replace(' ', '', $postcode));
	}
	static function format_postcode($postcode = '') {
		if (strlen($postcode) > 3) { 
			$postcode = substr($postcode, 0, strlen($postcode) - 3) . ' ' . substr($postcode, -3, 3);
		}
		return $postcode;
	}
}
	