<?php defined('SYSPATH') or die('No direct script access.');

class Model_Franchise extends ORM {
	protected $_has_many = array('franchise_available_products' => array(), 'franchise_users' => array(), 'templates' => array('through' => 'franchise_templates', 'foreign_key' => 'franchise_id'), 'franchise_products' => array(), 'customers' => array(), 'invoices' => array() );
		
	public function attributes($field = NULL, $id_prefix = '' ) {
		$attribs = array(
			'name'         		=> array('id' => $id_prefix . 'name'),
			'contactinfo'      	=> array('id' => $id_prefix . 'contactinfo'),
		);
		if ($field !== NULL) {
			return isset($attribs[$field]) ? $attribs[$field] : array();
		} else {
			return $attribs;
		}
	}
	

	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels($field = NULL)
	{
		$labels = array(
			'contactinfo'  => 'Contact Info',
		);
		return $field !== NULL ? (isset($labels[$field]) ? $labels[$field] : ucwords(Inflector::humanize($field))) : $labels;
	}

}
	