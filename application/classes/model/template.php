<?php defined('SYSPATH') or die('No direct script access.');
class Model_Template extends ORM {
	protected $_has_many = array('template_palettes' => array(), 'template_tags' => array('through' => 'template_tags_pivot', ), 'franchises' => array('through' => 'franchise_templates') );

	public function install($data) {
		try {
			// Save template data
			$this -> where('packagename', '=', $data['packagename']) -> where('version', '=', $data['version']) -> find();
			if ($this -> loaded()) {
				// Current version exists, replace it in the database
				$palettes = ORM::factory('template_palette') -> where('template_id', '=', $this -> id) -> find_all();
				// Delete colorpalettes
				foreach ($palettes as $palette) {
					// Remove colors
					DB::delete('template_colors') -> where('template_palette_id', '=', $palette -> id) -> execute();
					// Delete palette
					$palette -> delete();
				}
				// Unlink tags
				$this -> remove('template_tags');
			}
			$this -> values($data);
			$this -> date = DB::expr('CURDATE()');
			$this -> save();
		} catch (ORM_Validation_Exception $e) {
			$errors = $e -> errors();
			return false;
		}

		// Tags
		foreach ($data['tags'] as $tagItem) {
			try {
				$tag = ORM::factory('template_tag');
				$tag -> where('tag', '=', $tagItem) -> find();
				$tag -> tag = $tagItem;
				$tag -> save();
				// if (!$tag -> loaded())
				// return false;
				// Add tag to template
				$this -> add('template_tags', $tag);
			} catch (ORM_Validation_Exception $e) {
				$errors = $e -> errors();
				continue; // Not actually needed, but makes it read easier
			}
		}
		// Colors
		$paletteCount = 1;
		foreach ($data['palettes'] as $paletteItem) {
			$id = $paletteItem['id'];
			try {
				$palette = ORM::factory('template_palette');
				$palette -> template_id = $this -> id;
				$palette -> number = $paletteCount;
				$palette -> save();
			} catch (ORM_Validation_Exception $e) {
				$errors = $e -> errors();
				continue;
			}
			$colors = $paletteItem['colors'];
			$color_ids = array();
			foreach ($colors as $colorItem) {
				try {
					$color = ORM::factory('template_color');
					$color -> values($colorItem);
					$color -> template_palette_id = $palette -> id;
					$color -> save();
				} catch (ORM_Validation_Exception $e) {
					$errors = $e -> errors();
					continue;
				}
			}
			$paletteCount++;
		}
		echo Debug::vars($data);
		return true;
	}

}
