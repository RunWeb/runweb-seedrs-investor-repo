<?php defined('SYSPATH') or die('No direct script access.');

class Model_Franchise_Product extends ORM {
	protected $_belongs_to = array('franchise' => array(), 'product' => array(), 'customer_products' => array()); 
	// TODO: If any field is empty, inherit it from the appropriate parent product
		
	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels($field = NULL, $override = array())
	{
		$labels = array();
		//if (count($override))
		//	$labels = $override + $labels;
		
		return $field !== NULL ? (isset($labels[$field]) ? $labels[$field] : ucwords(Inflector::humanize($field))) : $labels;
	}
	
	public function attributes($field = NULL, $id_prefix = '' ) {
		$attribs = array(
			'name'         	=> array('id' => $id_prefix . 'name'),
			'description'   => array('id' => $id_prefix . 'description'),
			'price'         => array('id' => $id_prefix . 'price'),
			'enabled'       => array('id' => $id_prefix . 'enabled'),
		);
		if ($field !== NULL) {
			return isset($attribs[$field]) ? $attribs[$field] : array();
		} else {
			return $attribs;
		}
	}
	
	public function name() {
		if ($this -> name === NULL) {
			return $this -> product -> name;
		} else {
			return $this -> name;
		}
	}
	
	public function description() {
		if ($this -> description === NULL) {
			return $this -> product -> description;
		} else {
			return $this -> description;
		}
		
	}
	
	public function price() {
		if ($this -> price === NULL) {
			return $this -> baseprice();
		} else {
			return $this -> price;
		}
		
	}
	
	public function original_name() {
		if ($this -> name === NULL) {
			return $this -> name;
		} else {
			return $this -> name;
		}
		
	}
	
	public function original_description() {
		if ($this -> name === NULL) {
			return $this -> name;
		} else {
			return $this -> name;
		}
		
	}
	
	public function baseprice() {
		return $this -> product -> baseprice;
	}
	
	public function rrp() {
		return $this -> product -> rrp;
	}
	
	public function profit() {
		return $this -> price() - $this -> baseprice();
	}
	
	public function rrpdifference() {
		return $this -> rrp() - $this -> baseprice();
	}
	
	public function enabled() {
		return $this -> enabled && $this -> product -> enabled;
	}
}
	