<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Default auth user toke
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2011 Kohana Team
 * @license    http://kohanaframework.org/license
 */
class Model_Auth_Franchise_User_Token extends Model_Auth_Common_User_Token {

	// Relationships
	protected $_table_name = 'franchise_user_tokens';
	protected $_belongs_to = array('user' => array());

} // End Auth User Token Model