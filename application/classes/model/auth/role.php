<?php defined('SYSPATH') or die('No direct access allowed.');
/**

 * Default auth role
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */


if (IS_FRANCHISE) {
	class Model_Auth_Role extends Model_Auth_Franchise_Role {} 
}
if (IS_SHOP) {
	class Model_Auth_Role extends Model_Auth_Customer_Role {}
}
