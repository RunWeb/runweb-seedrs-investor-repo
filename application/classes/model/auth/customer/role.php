<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Default auth role
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Auth_Customer_Role extends Model_Auth_Common_Role {

	// Relationships
	protected $_table_name = 'customer_roles';
	protected $_has_many = array('users' => array('through' => 'customer_users_roles'));

} // End Auth Role Model