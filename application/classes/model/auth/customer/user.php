<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Default auth user
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2011 Kohana Team
 * @license    http://kohanaframework.org/license
 */
class Model_Auth_Customer_User extends Model_Auth_Common_User {

	protected $_table_name = 'customers';
	/**
	 * A user has many tokens and roles
	 *
	 * @var array Relationhips
	 */
	protected $_has_many = array(
		'user_tokens' => array('model' => 'user_token'),
		'roles' => array('model' => 'role', 'through' => 'customer_users_roles'),
		'bills' => array(),
		'customer_products' => array(),
		'customer_logs' => array(),
	);
	protected $_belongs_to = array('franchise' => array(), );	
	
	public function labels($field = NULL, $override = array()) {
		return parent::labels($field, $override);
	}
} // End Auth User Model