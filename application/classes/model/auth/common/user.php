<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Default auth user
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2011 Kohana Team
 * @license    http://kohanaframework.org/license
 */
class Model_Auth_Common_User extends ORM {

	/**
	 * Rules for the user model. Because the password is _always_ a hash
	 * when it's set,you need to run an additional not_empty rule in your controller
	 * to make sure you didn't hash an empty string. The password rules
	 * should be enforced outside the model or with a model helper method.
	 *
	 * @return array Rules
	 */
	public function rules()
	{
		return array(
			'username' => array(
				array('not_empty'),
				array('max_length', array(':value', 32)),
				array(array($this, 'unique'), array('username', ':value')),
			),
			'password' => array(
				array('not_empty'),
			),
			'email' => array(
				array('not_empty'),
				array('email'),
				array(array($this, 'unique'), array('email', ':value')),
			),
			'franchise_id' => array(
				array('not_empty'),
				array('digit')
			),
		);
	}

	/**
	 * Filters to run when data is set in this model. The password filter
	 * automatically hashes the password when it's set in the model.
	 *
	 * @return array Filters
	 */
	public function filters()
	{
		return array(
			'password' => array(
				array(array(Auth::instance(), 'hash'))
			),
			'personalpostcode' => array(
				 array('Filter::postcode', array(':value')),
			),
			'businesspostcode' => array(
				 array('Filter::postcode', array(':value')),
			),
		);
	}

	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels($field = NULL, $override = array())
	{
		$labels = array(
			'username'         => 'Username',
			'email'            => 'Email address',
			'password'         => 'Password',
	        'last_login'       => 'Last Login',
	        'telephone'        => 'Telephone',
	        'mobile'           => 'Mobile',
	        'fax'              => 'Fax',
	        'firstname'        => 'First Name',
	        'lastname'         => 'Last Name',
	        'companyname'      => 'Company Name',
	        'companyno'        => 'Company Number',
	        'personaladdress1' => 'Address',
	        'personaladdress2' => '',
	        'personaladdress3' => '',
	        'personaladdress4' => '',
	        'personalpostcode' => 'Postcode',
	        'businessaddress1' => 'Address',
	        'businessaddress2' => '',
	        'businessaddress3' => '',
	        'businessaddress4' => '',
	        'businesspostcode' => 'Postcode',
	        'notes' => 'Notes',
		);
		//if (count($override))
		//	$labels = $override + $labels;
		
		return $field !== NULL ? (isset($labels[$field]) ? $labels[$field] : ucwords(Inflector::humanize($field))) : $labels;
	}
	
	public function attributes($field = NULL, $id_prefix = '' ) {
		$attribs = array(
			'email'         	=> array('id' => $id_prefix . 'email'),
			'password'      	=> array('id' => $id_prefix . 'password'),
			'telephone'         => array('id' => $id_prefix . 'telephone'),
			'mobile'         	=> array('id' => $id_prefix . 'mobile'),
			'fax'         		=> array('id' => $id_prefix . 'fax'),
			'email'        		=> array('id' => $id_prefix . 'email'),
			'firstname'         => array('id' => $id_prefix . 'firstname'),
			'lastname'        	=> array('id' => $id_prefix . 'lastname'),
			'companyname'       => array('id' => $id_prefix . 'companyname'),
			'companyno'         => array('id' => $id_prefix . 'companyno'),
			'personaladdress1'  => array('id' => $id_prefix . 'personaladdress1'),
			'personaladdress2'  => array('id' => $id_prefix . 'personaladdress2'),
			'personaladdress3'  => array('id' => $id_prefix . 'personaladdress3'),
			'personaladdress4'  => array('id' => $id_prefix . 'personaladdress4'),
			'personalpostcode'  => array('id' => $id_prefix . 'personalpostcode'),
			'businessaddress1'  => array('id' => $id_prefix . 'businessaddress1'),
			'businessaddress2'  => array('id' => $id_prefix . 'businessaddress2'),
			'businessaddress3'  => array('id' => $id_prefix . 'businessaddress3'),
			'businessaddress4'  => array('id' => $id_prefix . 'businessaddress4'),
			'businesspostcode'  => array('id' => $id_prefix . 'businesspostcode'),
			'notes'             => array('id' => $id_prefix . 'notes'),
		);
		if ($field !== NULL) {
			return isset($attribs[$field]) ? $attribs[$field] : array();
		} else {
			return $attribs;
		}
	}
	/**
	 * Complete the login for a user by incrementing the logins and saving login timestamp
	 *
	 * @return void
	 */
	public function complete_login()
	{
		if ($this->_loaded)
		{
			// Update the number of logins
			$this->logins = new Database_Expression('logins + 1');

			// Set the last login date
			$this->last_login = time();

			// Save the user
			$this->update();
		}
	}

	/**
	 * Tests if a unique key value exists in the database.
	 *
	 * @param   mixed    the value to test
	 * @param   string   field name
	 * @return  boolean
	 */
	public function unique_key_exists($value, $field = NULL)
	{
		if ($field === NULL)
		{
			// Automatically determine field by looking at the value
			$field = $this->unique_key($value);
		}

		return (bool) DB::select(array('COUNT("*")', 'total_count'))
			->from($this->_table_name)
			->where($field, '=', $value)
			->where($this->_primary_key, '!=', $this->pk())
			->execute($this->_db)
			->get('total_count');
	}

	/**
	 * Allows a model use both email and username as unique identifiers for login
	 *
	 * @param   string  unique value
	 * @return  string  field name
	 */
	public function unique_key($value)
	{
		return Valid::email($value) ? 'email' : 'username';
	}

	/**
	 * Password validation for plain passwords.
	 *
	 * @param array $values
	 * @return Validation
	 */
	public static function get_password_validation($values)
	{
		return Validation::factory($values)
			->rule('password', 'min_length', array(':value', 8))
			->rule('password_confirm', 'matches', array(':validation', ':field', 'password'));
	}

	/**
	 * Create a new user
	 *
	 * Example usage:
	 * ~~~
	 * $user = ORM::factory('user')->create_user($_POST, array(
	 *	'username',
	 *	'password',
	 *	'email',
	 * );
	 * ~~~
	 *
	 * @param array $values
	 * @param array $expected
	 * @throws ORM_Validation_Exception
	 */
	public function create_user($values, $expected)
	{
		// Validation for passwords
		$extra_validation = Model_User::get_password_validation($values)
			->rule('password', 'not_empty');

		return $this->values($values, $expected)->create($extra_validation);
	}

	/**
	 * Update an existing user
	 *
	 * [!!] We make the assumption that if a user does not supply a password, that they do not wish to update their password.
	 *
	 * Example usage:
	 * ~~~
	 * $user = ORM::factory('user')
	 *	->where('username', '=', 'kiall')
	 *	->find()
	 *	->update_user($_POST, array(
	 *		'username',
	 *		'password',
	 *		'email',
	 *	);
	 * ~~~
	 *
	 * @param array $values
	 * @param array $expected
	 * @throws ORM_Validation_Exception
	 */
	public function update_user($values, $expected = NULL)
	{
		if (empty($values['password']))
		{
			unset($values['password'], $values['password_confirm']);
		}

		// Validation for passwords
		$extra_validation = Model_User::get_password_validation($values);

		return $this->values($values, $expected)->update($extra_validation);
	}

} // End Auth User Model