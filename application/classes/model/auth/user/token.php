<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Default auth user toke
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2011 Kohana Team
 * @license    http://kohanaframework.org/license
 */
if (IS_FRANCHISE) {
class Model_Auth_User_Token extends Model_Auth_Franchise_User_Token {}
}
if (IS_SHOP) {
class Model_Auth_User_Token extends Model_Auth_Customer_User_Token {}
}