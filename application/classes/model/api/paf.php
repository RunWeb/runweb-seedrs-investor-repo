<?php defined('SYSPATH') or die('No direct script access.');

class Model_Api_Paf extends Model {

	public function __construct($data = array()) {
		parent::__construct($data);
		$this -> set_data($data);
		$this->config = Kohana::$config->load('paf');
	}

	protected $data = array();

	public function get_data() {
		if (isset($_GET['debug']))
			echo "<br />Wee_API_PAF::get_data<br />\r\n";
		return $this -> data;
	}//	end get_data

	public function set_data($data = null) {
		if ($data === false) {
			$this -> data = array();
			return true;
		}
		if (!is_array($data))
			return false;

		$this -> data = $data;

		return true;
	}//	end set_data

	protected function build_url_from_postcode($postcode = '') {
		if (!is_string($postcode) OR strlen($postcode) <= 0)
			return false;
		if (isset($_GET['debug']))
			echo "build_url_from_postcode::postcode " . ($postcode) . "<br />\r\n";

		$postcode = trim(str_replace(' ', '', $postcode));
		if (!is_string($postcode) OR strlen($postcode) <= 0)
			return false;
		if (isset($_GET['debug']))
			echo "build_url_from_postcode::postcode " . ($postcode) . "<br />\r\n";

		$querystring = 'datakey=' . $this->config->password . '&postcode=' . $postcode;
		if (isset($_GET['debug']))
			echo "build_url_from_postcode::querystring " . ($querystring) . "<br />\r\n";

		$url = 'http://' . $this->config->summary_url . '?' . $querystring;
		//echo $url;
		return $url;
	}//	end build_url_from_postcode

	public function get_data_from_postcode($postcode = '') {
		$url = $this -> build_url_from_postcode($postcode);
		if (!is_string($url) OR strlen($url) <= 0)
			return false;
		if (isset($_GET['debug']))
			echo "get_addresses_from_postcode::url " . ($url) . "<br />\r\n";

		$fp = fopen($url, 'r');
		if (!is_resource($fp))
			return false;
		if (isset($_GET['debug']))
			echo "get_addresses_from_postcode::fp " . ($fp) . "<br />\r\n";

		##Parse the data 4096 bytes at a time##
		$json = '';
		while ($row = fread($fp, 4096))
			$json .= $row;
		if (isset($_GET['debug']))
			echo "get_addresses_from_postcode::json " . ($json) . "<br />\r\n";

		$data = json_decode($json, true);
		if ($data === NULL)
			return false;

		return $data;
	}//	end get_data_from_postcode

	public function get_addresses_from_postcode($postcode = '') {
		$data = $this -> get_data_from_postcode($postcode);
		if (!is_array($data) OR count($data) <= 0)
			return false;
		if (isset($_GET['debug']))
			echo "get_addresses_from_postcode::data " . count($data) . "<br />\r\n";

		// if (!isset($data['records']) OR !is_array($data['records']) OR count($data['records']) <= 0)
		// return false;
		//$records = $data['records'];
		// if (isset($_GET['debug'])) echo "get_addresses_from_postcode::records ". count($records) ."<br />\r\n";
		//
		// $addresses = array();
		// foreach ($records AS $record) {
		// if (!isset($record['l']) OR !is_string($record['l']) OR strlen($record['l']) <= 0)
		// continue;
		// if (!isset($record['id']) OR !is_string($record['id']) OR strlen($record['id']) <= 0)
		// continue;
		// $addresses[] = array(
		// 'id' => $record['id']
		// , 'name' => $record['l']
		// );
		// }
		//
		// if (!is_array($addresses) OR count($addresses) < 0)
		// return false;
		return $data;
	}

	protected function build_url_from_id($id = '') {
		if (!is_string($id) OR strlen($id) <= 0)
			return false;
		if (isset($_GET['debug']))
			echo "build_url_from_id::id " . ($id) . "<br />\r\n";

		$id = trim(str_replace(' ', '', $id));
		if (!is_string($id) OR strlen($id) <= 0)
			return false;
		if (isset($_GET['debug']))
			echo "build_url_from_id::id " . ($id) . "<br />\r\n";

		$querystring = '&datakey=' . $this->config->password . '&id=' . $id;
		if (isset($_GET['debug']))
			echo "build_url_from_id::querystring " . ($querystring) . "<br />\r\n";

		$url = 'http://' . $this->config->data_url . '?' . $querystring;

		return $url;
	}//	end build_url_from_id

	public function get_data_from_id($id = '') {
		$url = $this -> build_url_from_id($id);
		if (!is_string($url) OR strlen($url) <= 0)
			return false;
		if (isset($_GET['debug']))
			echo "get_data_from_id::url " . ($url) . "<br />\r\n";

		$fp = fopen($url, 'r');
		if (!is_resource($fp))
			return false;
		if (isset($_GET['debug']))
			echo "get_data_from_id::fp " . ($fp) . "<br />\r\n";

		##Parse the data 4096 bytes at a time##
		$json = '';
		while ($row = fread($fp, 4096))
			$json .= $row;
		if (isset($_GET['debug']))
			echo "get_data_from_id::json " . ($json) . "<br />\r\n";

		$data = json_decode($json, true);
		if ($data === NULL)
			return false;

		return $data;
	}//	end get_data_from_id

	public function get_address_from_id($id = '') {
		$data = $this -> get_data_from_id($id);
		if (!is_array($data) OR count($data) <= 0)
			return false;
		if (isset($_GET['debug']))
			echo "get_address_from_id::data " . print_r($data, true) . "<br />\r\n";

		if (!isset($data['found']) OR !is_numeric($data['found']) OR ($data['found']) < 0)
			return false;

		$address = array('organisation' => '', 'line1' => '', 'line2' => '', 'line3' => '', 'town' => '', 'county' => '', 'postcode' => '', 'country' => '');

		if (($data['found']) == 0)
			return $address;

		foreach ($address AS $datum => $value) {
			if (!isset($data[$datum]) OR !is_string($data[$datum]) OR strlen($data[$datum]) < 0)
				continue;
			$address[$datum] = $data[$datum];
		}

		if (!is_array($address) OR count($address) < 0)
			return false;
		return $address;
	}	//	end get_address_from_id

}	//	end Wee_API_PAF_Simply
