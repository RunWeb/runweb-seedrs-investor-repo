<?php defined('SYSPATH') or die('No direct script access.');

class Model_Api_Fasthost extends Model {


	protected $client = false;
	protected $session_id = false;
	protected $config = array();

	public function __construct() {
		$this->config = Kohana::$config->load('fasthost');
		$client = new SoapClient($this->config->url);
		if (!is_a($client, 'SoapClient')){
			return false;}
		$this->client = $client;

		$session_id = false;
		try {
			$session = $this->client->Login($this->config->username, $this->config->password, "", "");
		} catch (SoapFault $e) {
			exit($e->faultcode .' : '. $e->faultstring);	//	debug ONLY
			return false;
		}
		if (!$session){
			exit('Bad sess id ' . $session_id);
			return false;}
		$this->session = $session;
		
		$header = new SoapHeader("FasthostsAPI", "Session", $this->session);
		$this->client->__setSoapHeaders(array($header));
		return true;
	}
	
	public function createCustomer($customer) {
		if (!is_a($this->client, 'SoapClient')){
			return false;}
		$client = $this->client;
		$session = $this->session;
		try {
			$customer->ParentAccountNumber = $session->AccountNumber;
			$customer_id = $client->CreateAccount($customer);	
		} catch (SoapFault $e) {
			echo Debug::vars($e);
			return false;
		}
		return $customer_id;
	}
		
	public function getTLDs() {
		if (!is_a($this->client, 'SoapClient')){
			return false;}
		$client = $this->client;
		$session = $this->session;
		try	{
			$TLDs = $this->client->GetTLDsForRegistration();
		}
		catch (SoapFault $e) {
			Debug::vars($e);
			// unexpected error - any error would be an API error - handled on initialisation
			return false;
		}
		return $TLDs;
		 
	}
	
	public function getdomain($domainname) {
		if (!is_a($this->client, 'SoapClient')){
			return false;}
		$client = $this->client;
		$session = $this->session;
		try	{
			$domain = $this->client->GetDomain($domainname);
		}
		catch (SoapFault $e) {
			Debug::vars($e);
			// unexpected error - any error would be an API error - handled on initialisation
			return false;
		}
		return $domain;
		 
	}
	
	public function UpdateDomain($domain) {
		if (!is_a($this->client, 'SoapClient')){
			return false;}
		$client = $this->client;
		$session = $this->session;
		try	{
			$domain = $this->client->UpdateDomain($domain);
		}
		catch (SoapFault $e) {
			//print_r($e);
			// unexpected error - any error would be an API error - handled on initialisation
			return false;
		}
		return true;
		 
	}
	
	public function domain_suggestions($domain_name = '', $exclude = false) {
		if (!is_string($domain_name) OR strlen($domain_name) <= 0){
			return false;}
		if (!is_a($this->client, 'SoapClient')){
			return false;}
		$domain_name = strtolower($domain_name);
		$client = $this->client;
		$session = $this->session;
		try	{
			$DomainSuggestions = $this->client->GetDomainSuggestions($domain_name, $this->getTLDs(), 10);
		}
		catch (SoapFault $e)
		{
			// unexpected error
			//print_r($e);
			return false;
		}
		$out = array();
		if ($exclude){
			foreach ($DomainSuggestions as $k => $v) {
				if ($v != $domain_name) {
					$out[] = $v;
				}
			}
		}
		return $out;
	}
	
	public function buydomain($account, $domain_name = '') {
		if (!is_string($domain_name) OR strlen($domain_name) <= 0 OR strlen($account) <= 3){
			return false;}
		if (!is_a($this->client, 'SoapClient')){
			return false;}
		try {
		$cart = $this->client->CreateTestCart($account);
		// Register domain
		$this->client->RegisterDomain($account, $domain_name, 2, $cart);
		
		$order = $this->client->CheckoutCart($cart);
		} catch(SoapFault $e) {
			echo Debug::vars($e);
		}
		return $order;
	}

	public function is_domain_available($domain_name = '') {
		if (!is_string($domain_name) OR strlen($domain_name) <= 0){
			return false;}

		if (!is_a($this->client, 'SoapClient')){
			return false;}
		
		$domain_name = strtolower($domain_name);
		//$client = $this->client;
		//$session = $this->session;

		//$is_domain_available = false;
		// try {
			// $is_domain_available = $client->IsDomainAvailable($session_id, $domain_name);
		// } catch (SoapFault $e) {
			// //exit($e->faultcode .' : '. $e->faultstring);	//	debug ONLY
			// return false;
		// }
		
		try	{
			$DomainStatus = $this->client->GetDomainStatus($domain_name);
		}
		catch (SoapFault $e)
		{
			// print_r($e);
			// echo  $e->detail->APIError;
			// if ($error->Code == 101)
			// {
				// $ErrorMsg = "Please enter a valid domain name";
				// return false;
			// }
	
			// unexpected error
			return false;
		}
		return $DomainStatus->CanRegister;
	}


}	//	end Wee_API_Host_FastHost