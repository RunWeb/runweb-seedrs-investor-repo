<?php defined('SYSPATH') or die('No direct script access.');

class Model_Customer_Product extends ORM {
	protected $_belongs_to = array('customer' => array(), 'franchise_product' => array());	
	protected $_has_many = array('bill_lines' => array(), );
}
