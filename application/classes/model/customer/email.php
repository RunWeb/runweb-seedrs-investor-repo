<?php defined('SYSPATH') or die('No direct script access.' );

class Model_Customer_Email extends Model {

	public function annoyWill() {
		$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
			->setUsername('support@runweb.co.uk')
			->setPassword('NOPE');
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			// Give the message a subject
			->setSubject('Test email at ' . date("c"))
			// Set the From address with an associative array
			->setTo(array('info@runweb.co.uk' => 'info', 'info@runweb.co.uk' => 'info'))
			// Set the To addresses with an associative array
			->setFrom(array('support@runweb.co.uk' => 'RunWeb Support'))
			// Give it a body
			->setBody('Hello user')
			// And optionally an alternative body
			->addPart('<q>Hello <strong>USER</strong></q></td> </tr></tbody></table>', 'text/html')
			// Optionally add any attachments
			->attach(Swift_Attachment::fromPath('/var/www/vhosts/runweb.co.uk/shop.runweb.co.uk/new/donna.jpg'))
			;
		$mailer->send($message);
	}

}
	//	end Email
