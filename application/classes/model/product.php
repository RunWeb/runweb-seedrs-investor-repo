<?php defined('SYSPATH') or die('No direct script access.');

class Model_Product extends ORM {
	protected $_has_many = array('franchise_products' => array(),  'franchise_available_products' => array());
}
