<?php defined('SYSPATH') or die('No direct script access.');
class Model_Invoice extends ORM {
	protected $_belongs_to = array('franchise' => array(), );
	protected $_has_many = array('invoice_lines' => array(), );

	public function total() {
		if ($this -> loaded()) {
			$invoice = DB::select(DB::expr('SUM(price) as total')) -> from('invoice_lines') -> where('invoice_id', '=', $this -> id) -> execute();
			return sprintf("%.2f", (float)$invoice -> get('total') * (1 + $this -> vat / 100));
		}
	}

}
