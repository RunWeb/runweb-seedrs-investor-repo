<?php defined('SYSPATH') or die('No direct script access.');

class Model_Template_Palette extends ORM {

	protected $_belongs_to = array('template' => array(), );

	protected $_has_many = array('template_colors' => array(), );
}
