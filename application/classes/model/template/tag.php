<?php defined('SYSPATH') or die('No direct script access.');

class Model_Template_Tag extends ORM {
	protected $_has_many = array(
	    'templates' => array(
	        'model'   => 'template',
	        'through' => 'templates_tags_pivot',
	    ),
	);
}
