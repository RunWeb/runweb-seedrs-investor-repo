<?php defined('SYSPATH') or die('No direct script access.');
class Model_Bill extends ORM {
	protected $_belongs_to = array('customer' => array(), );
	protected $_has_many = array('bill_lines' => array(), );

	public function total() {
		if ($this -> loaded()) {
			$bill = DB::select(DB::expr('SUM(price) as total')) -> from('bill_lines') -> where('bill_id', '=', $this -> id) -> execute();
			return sprintf("%.2f", (float)$bill -> get('total') * (1 + $this -> vat / 100));
		}
	}

}
