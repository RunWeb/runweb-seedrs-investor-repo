<?php defined('SYSPATH') or die('No direct script access.');
class Model_Invoice_Line extends ORM {
	protected $_belongs_to = array('invoice' => array(), 'product' => array('foreign_key' => 'product_id', 'model' => 'customer_product',));

	public function total() {
		if ($this -> loaded()) {
			$invoice = DB::select(DB::expr('SUM(price) as total')) -> from('invoice_lines') -> where('invoice_id', '=', $this -> id) -> execute();
			return sprintf("%.2f", (float)$invoice -> get('total') * (1 + $this -> vat / 100));
		}
	}

}
