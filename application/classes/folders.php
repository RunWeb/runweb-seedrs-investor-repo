<?php defined('SYSPATH') or die('No direct script access.');

class Folders {
	static public function rrmdir($dir) {
		foreach (glob($dir . '/*') as $file) {
			if (is_dir($file))
				self::rrmdir($file);
			else
				unlink($file);
		}
		rmdir($dir);
	}

	static function mkdir($dir) {
		if (!is_dir($dir)) {
			$oldumask = umask(0);
			mkdir($dir, 0777);
			// so you get the sticky bit set
			umask($oldumask);
		}
	}

	static function copy($source, $destination) {
		self::mkdir($destination);
		$dir_handle = @opendir($source) or die("Unable to open");
		while ($file = readdir($dir_handle)) {
			if ($file != "." && $file != ".." && !is_dir("$source/$file"))
				copy("$source/$file", "$destination/$file");
			if ($file != "." && $file != ".." && is_dir("$source/$file"))//if it is folder
				self::copy("$source/$file", "$destination/$file");
		}
		closedir($dir_handle);
	}

}
