<?php defined('SYSPATH') or die('No direct script access.');

// Define site location
define('IS_SHOP', $_SERVER['SERVER_NAME'] == 'shop.runweb2.co.uk');
define('IS_FRANCHISE', $_SERVER['SERVER_NAME'] == 'franchise.runweb2.co.uk');


// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('Europe/London');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');



/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
	'base_url'   => '/',
	'index_file' => FALSE,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);


/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
		'auth'       	=> MODPATH.'auth',       // Basic authentication
	// 'cache'      => MODPATH.'cache',      // Caching with multiple backends
	    'codebench'  => MODPATH.'codebench',  // Benchmarking tool
		'database'   	=> MODPATH.'database',   // Database access
	// 'image'      => MODPATH.'image',      // Image manipulation
		'orm'        	=> MODPATH.'orm',        // Object Relationship Mapping
		'swiftmailer'   => MODPATH.'swiftmailer',        // Object Relationship Mapping
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
	// 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
	));

Cookie::$salt = 'lhdygLRwqn0PHOaKE868NsmrSMvL9AmxgV';
Session::$default = 'database';
/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */

Route::set('thumbs', 'thumbs(/<packagename>(/<version>(/<palette>(/<size>))))')
	  ->defaults(array(
	      'controller' => 'thumbs',
	      'action' => 'index',
	  )); 
Route::set('jsonapi', 'jsonapi(/<action>(/<id>))')
	  ->defaults(array(
	      'controller' => 'jsonapi',
	      'action' => 'index',
	  ));
	  // Replacing concrete connect
Route::set('concrete', 'concreteconnect(/<path>)', array('path'=>'.+'))
->defaults(array(
	'controller' => 'concreteconnect',
	'action'     => 'index',
));
if (IS_SHOP) {
	Route::set('default', '(<controller>(/<action>(/<id>)))', array('id'=>'.+'))
	->defaults(array(
		'controller' => 'shop',
		'action'     => 'index',
	));
}
if (IS_FRANCHISE) {

	Route::set('franchise_product', 'franchise/<franchise>/product/<product>(/<action>)', array('franchise'=>'\d+', 'product' => '\d+'))
	->defaults(array(
		'controller' => 'franchise_products',
		'action'     => 'index',
	));
	Route::set('franchise_products', 'franchise/<franchise>/products', array('franchise'=>'\d+'))
	->defaults(array(
		'controller' => 'franchise_products',
		'action'     => 'index',
	));
	
	Route::set('franchise_invoices', 'franchise/<franchise>/invoices', array('franchise'=>'\d+',))
	->defaults(array(
		'controller' => 'invoices',
		'action'     => 'index',
	));

	
	Route::set('franchise_invoice', 'franchise/<franchise>/invoice/<invoice>(/<action>)', array('franchise'=>'\d+', 'invoice' => '\d+'))
	->defaults(array(
		'controller' => 'invoices',
		'action'     => 'single',
	));

	Route::set('franchise_users', 'franchise/<franchise>/users', array('franchise'=>'\d+',))
	->defaults(array(
		'controller' => 'users',
		'action'     => 'index',
	));

	
	Route::set('franchise_user', 'franchise/<franchise>/user/<user>(/<action>)', array('franchise'=>'\d+', 'user' => '\d+'))
	->defaults(array(
		'controller' => 'users',
		'action'     => 'single',
	));

	Route::set('franchise_customers', 'franchise/<franchise>/customers', array('franchise'=>'\d+',))
	->defaults(array(
		'controller' => 'customers',
		'action'     => 'index',
	));

	
	Route::set('franchise_customer', 'franchise/<franchise>/customer/<customer>(/<action>)', array('franchise'=>'\d+', 'customer' => '\d+'))
	->defaults(array(
		'controller' => 'customers',
		'action'     => 'single',
	));

	Route::set('franchise_templates', 'franchise/<franchise>/templates(/<template>(/<action>))', array('franchise'=>'\d+', 'template' => '\d+'))
	->defaults(array(
		'controller' => 'templates',
		'action'     => 'index',
	));
			
	Route::set('franchises', 'franchise(/<id>(/<action>))', array('id'=>'\d+', 'action' => "(^products|edit)"))
	->defaults(array(
		'controller' => 'franchises',
		'action'     => 'single',
	));
	// Set the default at the end
	Route::set('default', '(<controller>(/<action>(/<id>)))', array('id'=>'.+'))
	->defaults(array(
		'controller' => 'franchise',
		'action'     => 'index',
	));
	

}

