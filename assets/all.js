$(function() {
	// $('#next').hide();
	// $('#back').hide();
	var data = domain_data || {};
	var domains = {
		'co.uk' : {
			years : [2], //
			price : 300
		},
		'org.uk' : {
			years : [2], //
			price : 300
		},
		'me.uk' : {
			years : [2], //
			price : 300
		},
		'ltd.uk' : {
			years : [2], //
			price : 300
		},
		'plc.uk' : {
			years : [2], //
			price : 300
		},
		'com' : {
			years : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], //
			blocks : false,
			price : 600
		},
		'co' : {
			years : [1, 2, 3, 4, 5], //
			blocks : false,
			price : 1900
		},
		'eu' : {
			years : [1], //
			price : 890
		},
		'org' : {
			years : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], //
			price : 950
		},
		'net' : {
			years : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], //
			price : 950
		},
		'info' : {
			years : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], //
			price : 890
		},
		'biz' : {
			years : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], //
			price : 950
		},
		'name' : {
			years : [1, 2, 10], //
			price : 890
		},
		'tv' : {
			years : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], //
			price : 3100
		},
		'mobi' : {
			years : [2, 5, 10], //
			price : 1500
		},
		'xxx' : {
			years : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], //
			price : 5000
		},

	};
	var domain = {
		timeout : 0,
		ajax : {
			suggestions : {},
			search : {},
			findAddress : {}
		},
		currentState : function(state) {
			// Checks the current page to ensure that the user is allowed to be here. If not, we move the user to the appropriate stage in the signup process
			//TODO: Correct state
			var states = {
				search : 1,
				time : 2,
				type : 3,
				details : 4,
				summary : 5
			};
			var stateNo = states.hasOwnProperty(state) ? states[state] : 1;
			if(stateNo > 1) {

			}
		},
		search : {
			init : function() {
				console.log('domain.search.init', arguments);
				// Fade others out - this one in
				document.title = 'RunWeb - Buy a domain - Search';
				$('section').not('#domain-search').fadeOut('fast');
				$('#domain-search').fadeIn('fast');
				// Change progress meter
				$('#progress').text('1 / 6');
				// Hide back button
				// $('#back').fadeOut('fast');
				$('#domain-search-failed').hide();
				$('#domain-search-failed-completely').hide();
				$('#domain-search-domain').keydown(function() {
					$('#domain-search-button').removeClass('disabled').prop('disabled', false);
				})
				$('#domain-search-button').removeClass('disabled').prop('disabled', false);
				// // Show next button
				// if(data.hasOwnProperty('domain') && data.domain != "") {
				// $('#next').fadeIn('fast');
				// $('#next a').attr('href', '#time');
				// $('#domain-search-domain').val(data.domain);
				// } else {
				// $('#next').fadeOut('fast');
				// }

				$('#domain-search-suggestions').hide();
				// If there are already suggestions, show them
				if($('#domain-search-suggestions').children().length > 0) {
					$('#domain-search-failed').show();
					$('#domain-search-failed h3').hide();
					$('#domain-search-suggestions').show();
				}

			},
			suggestions : function(jsonData) {
				console.log('domain.search.suggestions', arguments);
				$('#domain-search-suggestions').empty();
				if(jsonData.hasOwnProperty('suggestions') && jsonData.suggestions != false) {
					for(var i = 0; i < jsonData.suggestions.length; i++) {
						$('#domain-search-suggestions').append($('<li>', {
							'class' : 'button'
						}).text(jsonData.suggestions[i]).click(function(event) {
							data.domain = $(event.target).closest('li').text();
							$('#domain-search-domain').val(data.domain);
							$.history.load('time');
							domain.search.suggestions(jsonData);
						}));
					}
				}
				// Hide the loading box, show the suggestions
				var loading = $('#domain-search-failed-loading');
				if(loading.is(':visible')) {
					loading.hide();
					$('#domain-search-suggestions').show();
				}
			},
			success : function(jsonData) {
				console.log('domain.search.success', arguments);
				data.domain = jsonData.domain_name;
				$('#domain-search-domain').val(data.domain);
				$.history.load('time');
				// domain.search.suggestions(jsonData);
			},
			failure : function(jsonData) {
				console.log('domain.search.failure', arguments);
				$('.domain-name').text(jsonData.domain_name);
				// if(jsonData.hasOwnProperty('suggestions') && jsonData.suggestions != false) {
				var url = "jsonapi/domainsuggestions/" + jsonData.domain_name;
				//var url = 'delay.php';
				// The domain is not available, so show suggestions

				$('#domain-search-failed-loading').hide();
				$('#domain-search-failed, #domain-search-failed h3, #domain-search-failed-alternatives').show();

				domain.ajax.suggestions = $.getJSON(url, domain.search.suggestions);
				// } else {
				// $('#domain-search-failed-completely').fadeIn('slow');
				// }
			},
			submit : function(event) {
				console.log('domain.search.submit', arguments);
				if(arguments.length) {
					event.preventDefault();
				}
				// TODO: Prevent double submit
				$('#domain-search-failed-completely').fadeOut('fast');
				$('#domain-search-failed').hide();
				var DEBUG = true;
				var domainBox = $('#domain-search-domain');
				var domainName = domainBox.val().toLowerCase();
				if(!/^([A-Za-z0-9])+[A-Za-z0-9-]*([A-Za-z0-9])*\.?([A-Za-z0-9])*\.?([A-Za-z])*$/.test(domainName)) {
					// alert('invalid doname');
					domainBox.addClass('error');
					return false;
				} else {
					domainBox.removeClass('error');
				}
				$('#domain-search-button').prop('disabled', true);
				// $('#next').fadeOut('fast');
				var errorDomain = function(err) {

				}
				var domainResult = function(jsonData) {
					if(DEBUG)
						console.log(jsonData);

					$('#domain-search-domain').removeClass('searching');
					$('#domain-search-button').prop('disabled', false);
					if(jsonData.is_available) {
						domain.search.success(jsonData);
					} else {
						domain.search.failure(jsonData);
					}
				}

				domainBox.val(domainName);
				$('#domain-form-output').empty();
				event.preventDefault();
				// Prevent form submit
				if(DEBUG)
					console.log(domainName);
				if(domainName.indexOf('www.') == 0) {
					domainName = domainName.replace('www.', '');
					domainBox.val(domainName);
				}
				if(domainName.indexOf('.') == -1) {
					domainName += '.co.uk';
					domainBox.val(domainName);
				}
				if(DEBUG)
					console.log("checkDomain::domain_name " + domainName);
				if(domainName.length <= 0)
					errorDomain('Enter a domain name');
				var url = "jsonapi/domaincheck/" + domainName;
				// var url = 'delay.php';
				if(DEBUG)
					console.log("is_domain_available::url " + url);

				$('#domain-search-domain').addClass('searching');
				$('#domain-search-suggestions').hide().empty();
				if('abort' in domain.ajax.search) {
					domain.ajax.search.abort();
				}
				if('abort' in domain.ajax.suggestions) {
					domain.ajax.suggestions.abort();
				}
				domain.ajax.search = $.getJSON(url, domainResult);
				// Animate bar for search
				// searchBox.removeClass('searching', 100);
				// searchBox.addClass('searching', 100, domain.search.success);
			}
		},
		time : {
			init : function() {
				console.log('domain.time.init', arguments);

				document.title = 'RunWeb - Buy a domain - Time';
				if(!data.hasOwnProperty('domain') || data.domain == "") {
					$.history.load('search');
				}
				// Fade others out - this one in
				$('section').not('#domain-time').fadeOut('fast');
				$('#domain-time').fadeIn('fast');
				// Change progress meter
				$('#progress').text('2 / 6');
				// $('#back').show();
				// $('#back a').attr('href', '#search');
				if(data.hasOwnProperty('time') && data.time != "") {
					// $('#next').fadeIn('fast');
					// $('#next a').attr('href', '#type');
					// Selected div
					$('#domain-time input').each(function(index, element) {
						var label = $(element).closest('label');
						if($(element).val() == data.time) {
							label.addClass('selected');
						} else {
							label.removeClass('selected');
						}
					});
				} else {
					//$('#next').hide();
				}

				$('.domain-name').text(data.domain);
				// Note selection

			},
			submit : function(event) {
				console.log('domain.time.submit', arguments);
				if(arguments.length) {
					data.time = $(event.target).closest('label').children('input').val();
					event.preventDefault();
					$.history.load('type');
				}
			}
		},
		type : {
			init : function() {

				console.log('domain.type.init', arguments);

				document.title = 'RunWeb - Buy a domain - Type';
				if(!data.hasOwnProperty('time') || data.time == "") {
					if(!data.hasOwnProperty('search') || data.domain == "") {
						$.history.load('search');
					} else {
						$.history.load('time');
					}
				}

				$('section').not('#domain-type').fadeOut('fast');
				$('#domain-type').fadeIn('fast');
				// Change progress meter
				$('#progress').text('3 / 6');
				// $('#back').fadeIn('fast');
				// $('#back a').attr('href', '#time');
				if(data.hasOwnProperty('type') && data.type != "") {
					// $('#next').fadeIn('fast');
					// $('#next a').attr('href', '#details');
					// Selected div
					$('#domain-type input').each(function(index, element) {
						var label = $(element).closest('label');
						if($(element).val() == data.type) {
							label.addClass('selected');
						} else {
							label.removeClass('selected');
						}
					});
				} else {
					// $('#next').hide();
				}
			},
			submit : function(event) {
				console.log('domain.type.submit', arguments);
				if(arguments.length) {
					event.preventDefault();
					$.history.load('details');
					data.type = $('input', $(event.target).closest('label')).val();
				}
			}
		},
		details : {
			firstInit : true,
			init : function() {
				console.log('domain.details.init', arguments);

				document.title = 'RunWeb - Buy a domain - Details';
				// if(!data.hasOwnProperty('type') || data.type == "") {
				// if(!data.hasOwnProperty('time') || data.time == "") {
				// if(!data.hasOwnProperty('search') || data.domain == "") {
				// $.history.load('search');
				// } else {
				// $.history.load('time');
				// }
				// }
				// } else {
				// $.history.load('type');
				// }

				$('section').not('#domain-details').fadeOut('fast');
				$('#domain-details').fadeIn('fast');
				// $('#back').fadeIn('fast');
				// $('#back a').attr('href', '#type');
				// Change progress meter
				$('#progress').text('4 / 6');
				// if(data.hasOwnProperty('summary') && data.summary == true) {
				// $('#next').fadeIn('fast');
				// $('#next a').attr('href', '#summary');
				// } else {
				// $('#next').fadeOut('fast');
				// }
				// Check for personal
				if(data.type == 'business') {
					$('.type-personal').hide();
					$('.type-business').show();
				} else if(data.type == 'personal') {
					$('.type-business').hide();
					$('.type-personal').show();
				}

				// Check for address data

				if($('#domain-details-postcode').val() == "") {
					$('#domain-details-houseno, #domain-details-street, #domain-details-town, #domain-details-county').parent().hide();
					if(data.type == "business") {
						$('#domain-details-postcode').parent().hide();
					}
				}
				if(domain.details.firstInit) {
					$('#domain-details input').blur(domain.details.validate);
					domain.details.firstInit = false;
				}

			},
			validate : function(event) {
				var passed = false;
				var message = true
				var details = domain.details.getInput();
				var failed = [];
				var success = [];
				for(var field in details) {
					if(details.hasOwnProperty(field)) {
						if(details[field] !== false) {
							if(details[field] == "") {
								failed.push(field);
							} else {
								success.push(field);
							}
						}
					}
				}
				// If we have a (valid?) postcode, then perform a postcode search
				// Only show errors if we've clicked done
				if(!event && failed.length) {
					console.log(failed.join(', ') + ' are compulsory fields.');
					for(var i = 0; i < failed.length; i++) {
						var el = $('#domain-details-' + failed[i]);
						el.parent().addClass('error');
					}
				}

				if(success.length) {
					for(var i = 0; i < success.length; i++) {
						var el = $('#domain-details-' + success[i]);
						el.parent().removeClass('error');
					}
				}
				passed = !failed.length;
				// We don't want to return false to a keypress
				return !event ? passed : true;

			},
			getInput : function() {
				return {
					fullname : $('#domain-details-fullname').val(),
					email : $('#domain-details-email').val(),
					houseno : $('#domain-details-houseno').val(),
					street : $('#domain-details-street').val(),
					town : $('#domain-details-town').val(),
					county : $('#domain-details-county').val(),
					postcode : $('#domain-details-postcode').val(),
					companynameno : data.type == 'business' ? $('#domain-details-companynameno').val() : false
				};
			},
			submit : function(event) {
				console.log('domain.details.submit', arguments);
				if(arguments.length) {
					var details = domain.details.getInput();
					var valid = domain.details.validate(false);
					if(data.type == "personal" && details.postcode != "" && (details.street == "" || details.town == "" || details.county == "")) {
						domain.details.findAddress();
					}
					// if(data.type == "business" && details.companynanemno != "") {
						// domain.details.findCompany();
					// }
					if(!valid) {
						data.summary = false;
					} else {
						// Save data
						$.extend(data, details);
						// Load summary screen
						data.summary = true;
						$.history.load('summary');
					}
				}
			},
			update : function(details) {
				console.log('domain.details.update', arguments);
				for(var i in details) {
					if(details.hasOwnProperty(i)) {
						var el = $('#domain-details-' + i);
						el.val(details[i]);
						el.parent().fadeIn();
					}
				}
				$('#domain-details-houseno').val("").parent().fadeIn();
			},
			findAddress : function() {
				console.log('domain.details.findAddress', arguments);
				var postcode = $('#domain-details-postcode').addClass('searching').val();
				$('#domain-details-postcode').parent('div').addClass('searching');
				if('abort' in domain.ajax.findAddress) {
					domain.ajax.findAddress.abort();
				}
				if(!/[A-Z]{1,2}[0-9R][0-9A-Z]? ?[0-9][ABD-HJLNP-UW-Z]{2}/i.test(postcode)) {
					console.log('postcode failed validation');
					$('#domain-details-postcode').removeClass('searching').parent().addClass('error').parent('div').removeClass('searching');
					return false;
				} else {
					var url = "jsonapi/addresses/" + postcode;

					domain.ajax.findAddress = $.getJSON(url, function(json) {
						console.log('json_api:', arguments);
						$('#domain-details-postcode').removeClass('searching').parent('div').removeClass('searching');
						var foundAddress = json.hasOwnProperty('addresses') && json.addresses.hasOwnProperty('found') && json.addresses.found * 1 > 0;
						var items = ['street', 'town', 'county', 'houseno'];
						for(var i = 0; i < items.length; i++) {
							if(!foundAddress) {
								// No results returned
								$('#domain-details-' + items[i]).parent().fadeOut();
							} else {
								$('#domain-details-' + items[i]).parent().removeClass('error');
							}
						}
						if(!foundAddress) {
							$('#domain-details-postcode').parent().addClass('error');
							return;
						}

						var addressInfo = {
							street : json.addresses.line1,
							town : json.addresses.town,
							county : json.addresses.county,
							postcode : json.addresses.postcode
						}
						domain.details.update(addressInfo);
					});
					return domain.ajax.findAddress;
				}

			},
			findCompany : function() {
				// TODO: Display suggestion list?
				var company = $('#domain-details-companynameno').addClass('searching').val();
				var url = "/companieshouse/chxmlgateway/exNameSearch.php";
				$('#findCompany').hide();
				return $.getJSON(url, {
					name : $('#domain-details-companynameno').val()
				}, function(json) {
					console.log('companieshouse.php', arguments);
					$('#domain-details-companynameno').removeClass('searching');
					if(!json.hasOwnProperty('CompanyNumber') || !json.CompanyNumber) {
						// No results returned
						var items = ['street', 'town', 'county', 'houseno'];
						for(var i = 0; i < items.length; i++) {
							$('#domain-details-' + items[i]).parent().fadeOut();
						}
						// TODO: Error handling here? - make the div red or something
						return;
					}
					// TODO: Change boxes to address0,1,2,3,etc with the last one being postcode.
					var addressInfo = {
						companyname : json.CompanyName,
						companynameno : json.CompanyName,
						companyno : json.CompanyNumber,
						houseno : json.RegAddress.AddressLine[0],
						street : json.RegAddress.AddressLine[1],
						town : json.RegAddress.AddressLine[2],
						county : json.RegAddress.AddressLine[2],
						postcode : json.RegAddress.AddressLine[3]
					}
					domain.details.update(addressInfo);
				});
			}
		},
		summary : {
			init : function() {
				console.log('domain.summary.init', arguments);

				document.title = 'RunWeb - Buy a domain - Summary';
				// Change progress meter
				$('#progress').text('5 / 6');
				$('section').not('#domain-summary').fadeOut('fast');
				$('#domain-summary').fadeIn('fast');
				// $('#back').fadeIn('fast');
				// $('#back a').attr('href', '#details');
				//
				// $('#next').fadeOut('fast');

				// TODO: If any of the data isn't filled in - move back to the last page
				// Fill out fields
				console.log('data', data)
				for(var field in data) {
					if(data.hasOwnProperty(field)) {
						$('#domain-summary-' + field).text(data[field]);
					}
				}

				// Check for personal
				if(data.type == 'business') {
					console.log('show business bits');
					$('.type-personal').hide();
					$('.type-business').show();
				} else if(data.type == 'personal') {
					$('.type-business').hide();
					$('.type-personal').show();
				}
			},
			validate : function(event) {
				// TODO: Validation for this final step
				var passed = false;
				// var message = true
				var details = domain.summary.getInput();
				var failed = [];
				var success = [];
				for(var field in details) {
					if(details.hasOwnProperty(field)) {
						if(details[field] !== false) {
							if(details[field] == "") {
								failed.push(field);
							} else {
								success.push(field);
							}
						}
					}
				}
				// Only show errors if we've clicked done
				if(!event && failed.length) {
					console.log(failed.join(', ') + ' are compulsory fields.');
					for(var i = 0; i < failed.length; i++) {
						var el = $('#domain-billing-' + failed[i]);
						el.parent().addClass('error');
					}
				}

				if(success.length) {
					for(var i = 0; i < success.length; i++) {
						var el = $('#domain-billing-' + success[i]);
						el.parent().removeClass('error');
					}
				}
				passed = !failed.length;
				var terms = $('#domain-billing-terms').is(':checked');
				
				//passed = passed && terms;
				var $errorbox = $('#domain-billing-terms-message');
				
				if (!terms) {
					$errorbox.text("Terms must be accepted to continue").fadeIn();
				}
				if (!passed) {
					$errorbox.text("Compulsory fields required to continue").fadeIn();
				}
				passed = passed && terms;
				if (passed) {
					$errorbox.fadeOut();
				}
				// We don't want to return false to a keypress
				return !event ? passed : true;
			},
			getInput : function() {
				var chk = !$('#domain-billing-different').is(':checked');
				return {
					telephone : $('#domain-billing-telephone').val(),
					billinghouseno : chk ? '' : $('#domain-billing-houseno').val(),
					billingstreet : chk ? '' : $('#domain-billing-street').val(),
					billingtown : chk ? '' : $('#domain-billing-city').val(),
					billingcounty : chk ? '' : $('#domain-billing-county').val(),
					billingpostcode : chk ? '' : $('#domain-billing-postcode').val()
				};
			},
			submit : function(event) {
				console.log('domain.summary.submit', arguments);
				if(arguments.length) {
					var details = domain.summary.getInput();
					var valid = domain.summary.validate(false);
					if(!valid) {
						data.summary = false;
					} else {
						// Save data
						$.extend(data, details);
						// Load summary screen
						data.summary = true;
						$.history.load('thanks');
					}
				}
			},
			findAddress : function() {
				console.log('domain.summary.findAddress', arguments);
				var postcode = $('#domain-billing-postcode').addClass('searching').val();
				$('#domain-billing-postcode').parent('div').addClass('searching');
				if('abort' in domain.ajax.findAddress) {
					domain.ajax.findAddress.abort();
				}
				if(!/[A-Z]{1,2}[0-9R][0-9A-Z]? ?[0-9][ABD-HJLNP-UW-Z]{2}/i.test(postcode)) {
					console.log('postcode failed validation');
					$('#domain-billing-postcode').removeClass('searching').parent().addClass('error').parent('div').removeClass('searching');
					return false;
				} else {
					var url = "jsonapi/addresses/" + postcode;
					domain.ajax.findAddress = $.getJSON(url, function(json) {
						console.log('json_api:', arguments);
						$('#domain-billing-postcode').removeClass('searching').parent('div').removeClass('searching');
						var foundAddress = json.hasOwnProperty('addresses') && json.addresses.hasOwnProperty('found') && json.addresses.found * 1 > 0;
						var items = ['street', 'town', 'county', 'houseno'];
						for(var i = 0; i < items.length; i++) {
							if(!foundAddress) {
								// No results returned
								$('#domain-billing-' + items[i]).parent().fadeOut();
							} else {
								$('#domain-billing-' + items[i]).parent().removeClass('error');
							}
						}
						if(!foundAddress) {
							$('#domain-billing-postcode').parent().addClass('error');
							return;
						}

						var addressInfo = {
							street : json.addresses.line1,
							city : json.addresses.town,
							county : json.addresses.county,
							postcode : json.addresses.postcode
						}
						domain.summary.update(addressInfo);
					});
					return domain.ajax.findAddress;
				}

			},
			update : function(details) {
				console.log('domain.summary.update', arguments);
				for(var i in details) {
					if(details.hasOwnProperty(i)) {
						var el = $('#domain-billing-' + i);
						el.val(details[i]);
					}
				}
			}
		},
		thanks : {
			init : function() {
				// TODO: Display loading
				data.paid = 1;
				$.post("/kohana/shop/data", data);
				// TODO: When done, thanks + email etc
				$('#progress').text('6 / 6');
				$('section').not('#domain-thanks').fadeOut('fast');
				$('#domain-thanks').fadeIn('fast');
			}
		}
	};

	var loadContent = function(hash) {
		console.log('loadContent', arguments);
		if(domain.hasOwnProperty(hash)) {
			console.log(data);
			domain[hash].init();
			if (data.hasOwnProperty('domain')) {
				console.log(data);
				$.post("/kohana/shop/data", data);
			}
		}
	}

	$('#domain-search-failed-alternatives').click(function() {
		$('#domain-search-failed-alternatives').hide();
		$('#domain-search-failed').show();
		if($('#domain-search-suggestions').children().length > 0) {
			$('#domain-search-suggestions').show();
		} else {
			$('#domain-search-failed-loading').show();
		}
	});

	$.history.init(loadContent);
	$('form').submit(function(e) {
		e.preventDefault();
		var id = $(e.target).parent().attr('id').substr(7);
		if(domain.hasOwnProperty(id)) {
			domain[id].submit(e);
		}
	});

	$('section').hide();
	$('.year-selection').click(domain.time.submit);
	$('.type-selection').click(domain.type.submit);

	$('#findAddress').click(domain.details.findAddress);
	$('#findAddress2').click(domain.summary.findAddress);
	$('#findCompany').click(domain.details.findCompany);

	domain.search.init();
	$.history.load('search');
});
